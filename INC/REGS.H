/*----------------------------------------------------------------------------\
|  Src File:   regs.h                                                         |
|  Authored:   01/27/94, tgh                                                  |
|  Function:   Defines & prototypes for CPU registers.                        |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   01/27/94, tgh  -  Initial release.                             |
|                                                                             |
|           Copyright (c) 1993, 1994 GFI/USPS All Rights Reserved             |
\----------------------------------------------------------------------------*/
#ifndef  _REGS_H

#include  "gen.h"


/*
** -- SIM STRUCTURES --
*/
#option sep_on segment SIM_regs
extern union _smcr
{
  word w;
  struct
  {
    word exoff         : 1;
    word frzsw         : 1;
    word frzbm         : 1;
    word               : 1;
    word slven         : 1;
    word               : 1;
    word shen          : 2;
    word supv          : 1;
    word mm            : 1;
    word               : 2;
    word iarb          : 4;
  } b;
}smcr;

extern union _simtr
{
  word w;
  struct
  {
    word  mask         : 6;
    word               : 2;
    word  sosel        : 2;
    word  shirq        : 2;
    word  fbit         : 2;
    word  bwc          : 2;
  } b;
}simtr;

extern union _syncr
{
  word w;
  struct
  {
    word  w            : 1;
    word  x            : 1;
    word  y            : 6;
    word  ediv         : 1;
    word               : 2;
    word  slimp        : 1;
    word  slock        : 1;
    word  rsten        : 1;
    word  stsim        : 1;
    word  stext        : 1;
  } b;
}syncr;

extern union _rsr
{
  word w;
  struct
  {
    word               : 8;
    word  ext          : 1;
    word  pow          : 1;
    word  sw           : 1;
    word  hlt          : 1;
    word               : 1;
    word  loc          : 1;
    word  sys          : 1;
    word  tst          : 1;
  } b;
}rsr;

extern word simtre;          /* not defined - test only */
extern word unused_1[];

extern union _porte_1
{
  word w;
  struct
  {
    word                : 8;
    word  bit7          : 1;
    word  bit6          : 1;
    word  bit5          : 1;
    word  bit4          : 1;
    word  bit3          : 1;
    word  bit2          : 1;
    word  bit1          : 1;
    word  bit0          : 1;
  } b;
}porte_1;

extern union _porte_2
{
  word w;
  struct
  {
    word                : 8;
    word  bit7          : 1;
    word  bit6          : 1;
    word  bit5          : 1;
    word  bit4          : 1;
    word  bit3          : 1;
    word  bit2          : 1;
    word  bit1          : 1;
    word  bit0          : 1;
  } b;
}porte_2;

extern union _ddre
{
  word w;
  struct
  {
    word                : 8;
    word  dde7          : 1;
    word  dde6          : 1;
    word  dde5          : 1;
    word  dde4          : 1;
    word  dde3          : 1;
    word  dde2          : 1;
    word  dde1          : 1;
    word  dde0          : 1;
  } b;
}ddre;

extern union _pepar
{
  word w;
  struct
  {
    word                : 8;
    word  pepa7         : 1;
    word  pepa6         : 1;
    word  pepa5         : 1;
    word  pepa4         : 1;
    word  pepa3         : 1;
    word  pepa2         : 1;
    word  pepa1         : 1;
    word  pepa0         : 1;
  } b;
}pepar;

extern union _portf_1
{
  word w;
  struct
  {
    word                : 8;
    word  bit7          : 1;
    word  bit6          : 1;
    word  bit5          : 1;
    word  bit4          : 1;
    word  bit3          : 1;
    word  bit2          : 1;
    word  bit1          : 1;
    word  bit0          : 1;
  } b;
}portf_1;

extern union _portf_2
{
  word w;
  struct
  {
    word                : 8;
    word  bit7          : 1;
    word  bit6          : 1;
    word  bit5          : 1;
    word  bit4          : 1;
    word  bit3          : 1;
    word  bit2          : 1;
    word  bit1          : 1;
    word  bit0          : 1;
  } b;
}portf_2;

extern union _ddrf
{
  word w;
  struct
  {
    word                : 8;
    word  ddf7          : 1;
    word  ddf6          : 1;
    word  ddf5          : 1;
    word  ddf4          : 1;
    word  ddf3          : 1;
    word  ddf2          : 1;
    word  ddf1          : 1;
    word  ddf0          : 1;
  } b;
}ddrf;

extern union _pfpar
{
  word w;
  struct
  {
    word                : 8;
    word  pfpa7         : 1;
    word  pfpa6         : 1;
    word  pfpa5         : 1;
    word  pfpa4         : 1;
    word  pfpa3         : 1;
    word  pfpa2         : 1;
    word  pfpa1         : 1;
    word  pfpa0         : 1;
  } b;
}pfpar;

extern union _sypcr
{
  word w;
  struct
  {
    word                : 8;
    word  swe           : 1;
    word  swp           : 1;
    word  swt           : 2;
    word  hme           : 1;
    word  bme           : 1;
    word  bmt           : 2;
  } b;
}sypcr;

extern union _picr
{
  word w;
  struct
  {
    word                : 5;
    word  pirql         : 3;
    word  piv           : 8;
  } b;
}picr;

extern union _pitr
{
  word w;
  struct
  {
    word                : 7;
    word  ptp           : 1;
    word  pitr          : 8;
  } b;
}pitr;

extern union _swsr
{
  word w;
  struct
  {
    word                : 8;
    word  swsr          : 8;
  } b;
}swsr;

extern word unused_2[];
extern word tstmsra;         /* 16 bit contiguous register */
extern word tstmsrb;         /* 16 bit contiguous register */
extern word tstsc;           /* 16 bit contiguous register */
extern word tstrc;           /* 16 bit contiguous register */

extern union _creg
{
  word w;
  struct
  {
    word  busy          : 1;
    word  tmarm         : 1;
    word  comp          : 1;
    word  imbtst        : 1;
    word  cputr         : 1;
    word  qbit          : 1;
    word  muxsel        : 1;
    word                : 4;
    word  acut          : 1;
    word  scont         : 1;
    word  sshop         : 1;
    word  sato          : 1;
    word  etm           : 1;
  } b;
}creg;

extern union _dreg
{
  word w;
  struct
  {
    word                : 5;
    word  wait3_1       : 3;
    word  msra18_16     : 3;
    word  msrac         : 1;
    word  msrb18_16     : 3;
    word  msrbc         : 1;
  } b;
}dreg;

extern word unused_3[];

extern union _portc
{
  word w;
  struct
  {
    word  unused        : 8;
    word  bit7          : 1;
    word  bit6          : 1;
    word  bit5          : 1;
    word  bit4          : 1;
    word  bit3          : 1;
    word  bit2          : 1;
    word  bit1          : 1;
    word  bit0          : 1;
  } b;
}portc;

extern word unused_4;

extern union _cspar0
{
  word w;
  struct
  {
    word                : 2;
    word  cs5           : 2;
    word  cs4           : 2;
    word  cs3           : 2;
    word  cs2           : 2;
    word  cs1           : 2;
    word  cs0           : 2;
    word  csboot        : 2;
  } b;
}cspar0;

extern union _cspar1
{
  word w;
  struct
  {
    word                : 6;
    word  cs10          : 2;
    word  cs9           : 2;
    word  cs8           : 2;
    word  cs7           : 2;
    word  cs6           : 2;
  } b;
}cspar1;

extern union _csbarbt
{
  word w;
  struct
  {
    word  a23_11        : 13;
    word  block_size    : 3;
  } b;
}csbarbt;

extern union _csorbt
{
  word w;
  struct
  {
    word  mode          : 1;
    word  byt           : 2;
    word  r_w           : 2;
    word  strb          : 1;
    word  dsack         : 4;
    word  space         : 2;
    word  ipl           : 3;
    word  avec          : 1;
  } b;
}csorbt;

extern union _csbar0
{
  word w;
  struct
  {
    word  a23_11        : 13;
    word  block_size    : 3;
  } b;
}csbar0;

extern union _csor0
{
  word w;
  struct
  {
    word  mode          : 1;
    word  byt           : 2;
    word  r_w           : 2;
    word  strb          : 1;
    word  dsack         : 4;
    word  space         : 2;
    word  ipl           : 3;
    word  avec          : 1;
  } b;
}csor0;

extern union _csbar1
{
  word w;
  struct
  {
    word  a23_11        : 13;
    word  block_size    : 3;
  } b;
}csbar1;

extern union _csor1
{
  word w;
  struct
  {
    word  mode          : 1;
    word  byt           : 2;
    word  r_w           : 2;
    word  strb          : 1;
    word  dsack         : 4;
    word  space         : 2;
    word  ipl           : 3;
    word  avec          : 1;
  } b;
}csor1;

extern union _csbar2
{
  word w;
  struct
  {
    word  a23_11        : 13;
    word  block_size    : 3;
  } b;
}csbar2;

extern union _csor2
{
  word w;
  struct
  {
    word  mode          : 1;
    word  byt           : 2;
    word  r_w           : 2;
    word  strb          : 1;
    word  dsack         : 4;
    word  space         : 2;
    word  ipl           : 3;
    word  avec          : 1;
  } b;
}csor2;

extern union _csbar3
{
  word w;
  struct
  {
    word  a23_11        : 13;
    word  block_size    : 3;
  } b;
}csbar3;

extern union _csor3
{
  word w;
  struct
  {
    word  mode          : 1;
    word  byt           : 2;
    word  r_w           : 2;
    word  strb          : 1;
    word  dsack         : 4;
    word  space         : 2;
    word  ipl           : 3;
    word  avec          : 1;
  } b;
}csor3;

extern union _csbar4
{
  word w;
  struct
  {
    word  a23_11        : 13;
    word  block_size    : 3;
  } b;
}csbar4;

extern union _csor4
{
  word w;
  struct
  {
    word  mode          : 1;
    word  byt           : 2;
    word  r_w           : 2;
    word  strb          : 1;
    word  dsack         : 4;
    word  space         : 2;
    word  ipl           : 3;
    word  avec          : 1;
  } b;
}csor4;

extern union _csbar5
{
  word w;
  struct
  {
    word  a23_11        : 13;
    word  block_size    : 3;
  } b;
}csbar5;

extern union _csor5
{
  word w;
  struct
  {
    word  mode          : 1;
    word  byt           : 2;
    word  r_w           : 2;
    word  strb          : 1;
    word  dsack         : 4;
    word  space         : 2;
    word  ipl           : 3;
    word  avec          : 1;
  } b;
}csor5;

extern union _csbar6
{
  word w;
  struct
  {
    word  a23_11        : 13;
    word  block_size    : 3;
  } b;
}csbar6;

extern union _csor6
{
  word w;
  struct
  {
    word  mode          : 1;
    word  byt           : 2;
    word  r_w           : 2;
    word  strb          : 1;
    word  dsack         : 4;
    word  space         : 2;
    word  ipl           : 3;
    word  avec          : 1;
  } b;
}csor6;

extern union _csbar7
{
  word w;
  struct
  {
    word  a23_11        : 13;
    word  block_size    : 3;
  } b;
}csbar7;

extern union _csor7
{
  word w;
  struct
  {
    word  mode          : 1;
    word  byt           : 2;
    word  r_w           : 2;
    word  strb          : 1;
    word  dsack         : 4;
    word  space         : 2;
    word  ipl           : 3;
    word  avec          : 1;
  } b;
}csor7;

extern union _csbar8
{
  word w;
  struct
  {
    word  a23_11        : 13;
    word  block_size    : 3;
  } b;
}csbar8;

extern union _csor8
{
  word w;
  struct
  {
    word  mode          : 1;
    word  byt           : 2;
    word  r_w           : 2;
    word  strb          : 1;
    word  dsack         : 4;
    word  space         : 2;
    word  ipl           : 3;
    word  avec          : 1;
  } b;
}csor8;

extern union _csbar9
{
  word w;
  struct
  {
    word  a23_11        : 13;
    word  block_size    : 3;
  } b;
}csbar9;

extern union _csor9
{
  word w;
  struct
  {
    word  mode          : 1;
    word  byt           : 2;
    word  r_w           : 2;
    word  strb          : 1;
    word  dsack         : 4;
    word  space         : 2;
    word  ipl           : 3;
    word  avec          : 1;
  } b;
}csor9;

extern union _csbar10
{
  word w;
  struct
  {
    word  a23_11        : 13;
    word  block_size    : 3;
  } b;
}csbar10;

extern union _csor10
{
  word w;
  struct
  {
    word  mode          : 1;
    word  byt           : 2;
    word  r_w           : 2;
    word  strb          : 1;
    word  dsack         : 4;
    word  space         : 2;
    word  ipl           : 3;
    word  avec          : 1;
  } b;
}csor10;
#option sep_off


/*
** -- QSM STRUCTURES --
*/
#option sep_on segment QSM_regs
extern union _qmcr
{
  word w;
  struct
  {
    word  stop          : 1;
    word  frz           : 2;
    word                : 5;
    word  supv          : 1;
    word                : 3;
    word  iarb          : 4;
  } b;
}qmcr;

extern union _qtest
{
  word w;
  struct
  {
    word                : 12;
    word  tsbd          : 1;
    word  sync          : 1;
    word  tqsm          : 1;
    word  tmm           : 1;
  } b;
}qtest;

extern union _qilr_qivr
{
  word w;
  struct
  {
    word                : 2;
    word  ilqspi        : 3;
    word  ilsci         : 3;
    word  intv          : 8;
  } b;
}qilr_qivr;

extern word qsm_rsvd_1;

extern union _sccr0
{
  word w;
  struct
  {
    word                : 3;
    word  scbr          : 13;
  } b;
}sccr0;

extern union _sccr1
{
  word w;
  struct
  {
    word                : 1;
    word  loops         : 1;
    word  woms          : 1;
    word  ilt           : 1;
    word  pt            : 1;
    word  pe            : 1;
    word  m             : 1;
    word  wake          : 1;
    word  tie           : 1;
    word  tcie          : 1;
    word  rie           : 1;
    word  ilie          : 1;
    word  te            : 1;
    word  re            : 1;
    word  rwu           : 1;
    word  sbk           : 1;
  } b;
}sccr1;

extern union _scsr
{
  word w;
  struct
  {
    word                : 7;
    word  tdre          : 1;
    word  tc            : 1;
    word  rdrf          : 1;
    word  raf           : 1;
    word  idle          : 1;
    word  or            : 1;
    word  nf            : 1;
    word  fe            : 1;
    word  pf            : 1;
  } b;
}scsr;

extern union _scdr
{
  word w;
  struct
  {
    word                : 7;
    word  r_t           : 9;
  } b;
}scdr;

extern word qsm_rsvd_2;
extern word qsm_rsvd_3;

extern union _qpdr
{
  word w;
  struct
  {
    word                : 8;
    word  txd           : 1;
    word  pcs3          : 1;
    word  pcs2          : 1;
    word  pcs1          : 1;
    word  pcs0          : 1;
    word  sck           : 1;
    word  mosi          : 1;
    word  miso          : 1;
  } b;
}qpdr;

extern union _qpar_qddr
{
  word w;
  struct
  {
    word                : 1;
    word  pcs3          : 1;
    word  pcs2          : 1;
    word  pcs1          : 1;
    word  pcs0          : 1;
    word                : 1;
    word  mosi          : 1;
    word  miso          : 1;
    word  _txd          : 1;
    word  _pcs3         : 1;
    word  _pcs2         : 1;
    word  _pcs1         : 1;
    word  _pcs0         : 1;
    word  _sck          : 1;
    word  _mosi         : 1;
    word  _miso         : 1;
  } b;
}qpar_qddr;

extern union _spcr0
{
  word w;
  struct
  {
    word  mstr          : 1;
    word  womq          : 1;
    word  bits          : 4;
    word  cpol          : 1;
    word  cpha          : 1;
    word  baud          : 8;
  } b;
}spcr0;

extern union _spcr1
{
  word w;
  struct
  {
    word  spe           : 1;
    word  dsclk         : 7;
    word  dtl           : 8;
  } b;
}spcr1;

extern union _spcr2
{
  word w;
  struct
  {
    word  spifie        : 1;
    word  wren          : 1;
    word  wrto          : 1;
    word                : 1;
    word  endqp         : 4;
    word                : 4;
    word  newqp         : 4;
  } b;
}spcr2;

extern union _spcr3_spsr
{
  word w;
  struct
  {
    word                : 5;
    word  loopq         : 1;
    word  hmie          : 1;
    word  halt          : 1;
    word  spif          : 1;
    word  modf          : 1;
    word  halta         : 1;
    word                : 1;
    word  cptqp         : 4;
  } b;
}spcr3_spsr;
#option sep_off


/*
** -- QSM QUEUE RAM --
*/
#option sep_on segment QSPI_qram
extern word rec_ram[];
extern word tran_ram[];
extern byte comd_ram[];
#option sep_off


/*
** -- GPT REGISTERS --
*/
#option sep_on segment GPT_regs
extern union _gmcr
{
  word w;
  struct
  {
    word  stop          : 1;
    word  frz1          : 1;
    word  frz0          : 1;
    word  stopp         : 1;
    word  incp          : 1;
    word                : 3;
    word  supv          : 1;
    word                : 3;
    word  iarb          : 4;
  } b;
}gmcr;

extern word gpt_rsv_1;

extern union _icr
{
  word w;
  struct
  {
    word  pab           : 4;
    word                : 1;
    word  irl           : 3;
    word  ivba          : 4;
    word                : 4;
  } b;
}icr;

extern union _pddr_pdr
{
  word w;
  struct
  {
    word  ddri4o5       : 1;
    word  ddro4         : 1;
    word  ddro3         : 1;
    word  ddro2         : 1;
    word  ddro1         : 1;
    word  ddri3         : 1;
    word  ddri2         : 1;
    word  ddri1         : 1;
    word  ic4_oc5       : 1;
    word  oc4           : 1;
    word  oc3           : 1;
    word  oc2           : 1;
    word  oc1           : 1;
    word  ic3           : 1;
    word  ic2           : 1;
    word  ic1           : 1;
  } b;
}pddr_pdr;

extern union _oc1m_oc1d
{
  word w;
  struct
  {
    word  oc1m7         : 1;
    word  oc1m6         : 1;
    word  oc1m5         : 1;
    word  oc1m4         : 1;
    word  oc1m3         : 1;
    word                : 3;
    word  oc1d7         : 1;
    word  oc1d6         : 1;
    word  oc1d5         : 1;
    word  oc1d4         : 1;
    word  oc1d3         : 1;
    word                : 3;
  } b;
}oc1m_oc1d;

extern word tcnt;            /* 16 bit timer counter reg */

extern union _pactl_pacnt
{
  word w;
  struct
  {
    word  pais          : 1;
    word  paen          : 1;
    word  pamod         : 1;
    word  pedge         : 1;
    word  pclks         : 1;
    word  i4_o5         : 1;
    word  packl1        : 1;
    word  packl0        : 1;
    word  p_a_timer     : 8;
  } b;
}pactl_pacnt;

extern word tic1;
extern word tic2;
extern word tic3;
extern word toc1;
extern word toc2;
extern word toc3;
extern word toc4;
extern word ti4o5;

extern union _tctl1_tctl2
{
  word w;
  struct
  {
    word  o5            : 2;
    word  o4            : 2;
    word  o3            : 2;
    word  o2            : 2;
    word  edge4         : 2;
    word  edge3         : 2;
    word  edge2         : 2;
    word  edge1         : 2;
  } b;
}tctl1_tctl2;

extern union _tmsk1_tmsk2
{
  word w;
  struct
  {
    word  i4o5i         : 1;
    word  oc4i          : 1;
    word  oc3i          : 1;
    word  oc2i          : 1;
    word  oc1i          : 1;
    word  ic3i          : 1;
    word  ic2i          : 1;
    word  ic1i          : 1;
    word  toi           : 1;
    word                : 1;
    word  paovi         : 1;
    word  paii          : 1;
    word  cprout        : 1;
    word  cpr           : 3;
  } b;
}tmsk1_tmsk2;

extern union _tflg1_tflg2
{
  word w;
  struct
  {
    word  i4o5f         : 1;
    word  oc4f          : 1;
    word  oc3f          : 1;
    word  oc2f          : 1;
    word  oc1f          : 1;
    word  ic3f          : 1;
    word  ic2f          : 1;
    word  ic1f          : 1;
    word  tof           : 1;
    word                : 1;
    word  paovf         : 1;
    word  paif          : 1;
    word                : 4;
  } b;
}tflg1_tflg2;

extern union _cforc_pwmc
{
  word w;
  struct
  {
    word  foc5          : 1;
    word  foc4          : 1;
    word  foc3          : 1;
    word  foc2          : 1;
    word  foc1          : 1;
    word                : 1;
    word  fpwma         : 1;
    word  fpwmb         : 1;
    word  pprout        : 1;
    word  ppr           : 3;
    word  sfa           : 1;
    word  sfb           : 1;
    word  f1a           : 1;
    word  f1b           : 1;
  } b;
}cforc_pwmc;

extern word pwma_pwmb;
extern word pwmcnt;
extern word pwmab_buf;
extern word prescaler;
extern word pwm_rsvd;
#option sep_off


void  Initialize_Qsm( void );
void  Initialize_Gpt( void );
void  Initialize_PortF( void );

#define  _REGS_H
#endif
