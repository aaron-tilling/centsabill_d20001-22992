/*----------------------------------------------------------------------------\
|  Src File:   menuproc.h                                                     |
|  Authored:   04/29/94, cj, rz                                               |
|  Function:   Defines & prototypes for menu processing.                      |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   09/20/94, tgh  -  Initial release.                             |
|      2.00:   08/11/01, aat  -  modified for use in farebox                  |
|                                                                             |
|           Copyright (c) 1993, 1994 GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
#ifndef  _MENUPROC_H
#define  _MENUPROC_H

#define MENU_NULL 0x00
#define MENU_LOAD 0x01
#define MENU_EXIT 0x02
#define MENU_EXEC 0x03

#define MENU(m)   static FbxMenu m[] = {
#define MENU_END  { NULL, MENU_NULL, ( void(*)() )NULL }};

typedef struct
{
   char *tag;
   char op;
   void *funct;
}  FbxMenu;

extern int MenuProc( FbxMenu * );
extern int MakeCardSuper;
extern int MakeCardAgency;

#endif /*_MENUPROC_H*/
