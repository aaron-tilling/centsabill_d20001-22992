*-----------------------------------------------------------------------------+
*  Src File:   gen.inc                                                        |
*  Authored:   01/31/94, tgh                                                  |
*  Function:   Generic include file for assembler modules.                    |
*  Comments:                                                                  |
*                                                                             |
*  Revision:                                                                  |
*      1.00:   01/31/94, tgh  -  Initial release.                             |
*      2.00:   03/24/94, tgh  -  pending                                      |
*                                                                             |
*           Copyright (c) 1993, 1994 GFI/USPS All Rights Reserved             |
*-----------------------------------------------------------------------------+

*-----------------------------------------------------------------------------+
*                       Include Files                                         |
*-----------------------------------------------------------------------------+
        nolist

* Define function label and make it public
* usage: _fnct <name>
*
        _fnct:          macro
                xdef    \1
                \1:
                        endm

        bit0:   equ     0000h
        bit1:   equ     0001h
        bit2:   equ     0002h
        bit3:   equ     0004h
        bit4:   equ     0010h
        bit5:   equ     0020h
        bit6:   equ     0040h
        bit7:   equ     0080h
        bit8:   equ     0100h
        bit9:   equ     0200h
        bit10:  equ     0400h
        bit11:  equ     0800h
        bit12:  equ     1000h
        bit13:  equ     2000h
        bit14:  equ     4000h
        bit15:  equ     8000h

        .bit0:  equ     0
        .bit1:  equ     1
        .bit2:  equ     2
        .bit3:  equ     3
        .bit4:  equ     4
        .bit5:  equ     5
        .bit6:  equ     6
        .bit7:  equ     7

        led_four:  equ  0008h
        led_three: equ  0004h
        led_two:   equ  0002h
        led_one:   equ  0001h 
                 

* Start :::::::::  Debugging Macros  ::::::::::
*       Display a character on the display
*       Display_Putchar <character> (if no argument the d0 holds value)
*       ex. Display_Putchar T   will transmit 'T' via the 8-char. display
        Display_Putchar:        macro
                        move.l  d0,-(sp)
                ifnc '\1',''
                        move.b  #'\1',d0
                endc
                        move.b  d0,090818h+\2
                        move.l  (sp)+,d0
                exit\@:
                        endm

* Convert a binary byte in d0 to displayable hex
        _to_hex:        macro
           ifnc '\1',''
                        move.b  \1,d0
           endc
                        or.b    #30h,d0
                        cmpi.b  #39h,d0
                        bls.s   l1\@
                        addq.b  #7,d0
                l1\@:
                        endm

* Toggle an led on the USPS 331 board.
        _toggle_led:    macro
                        xref _XorLed
                        move.l  d0,-(sp)
           ifnc '\1',''
                        move.b  #\1,d0
           endc
                        move.w  d0,-(sp)
                        jsr     _XorLed
                        addq.w  #2,sp
                        move.l  (sp)+,d0
                        endm

* Set an led on the USPS 331 board.
        _set_led:       macro
                        xref _SetLed
                        move.l  d0,-(sp)
           ifnc '\1',''
                        move.b  #\1,d0
           endc
                        move.w  d0,-(sp)
                        jsr     _SetLed
                        addq.w  #2,sp
                        move.l  (sp)+,d0
                        endm

* Clr an led on the USPS 331 board.
        _clr_led:       macro
                        xref _ClrLed
                        move.l  d0,-(sp)
           ifnc '\1',''
                        move.b  #\1,d0
           endc
                        move.w  d0,-(sp)
                        jsr     _ClrLed
                        addq.w  #2,sp
                        move.l  (sp)+,d0
                        endm

*       Transmit a character via diag. port for debugging
*       Putchar <character> (if no argument the d0 holds value)
*       ex. Putchar T   will transmit 'T' via the diag. port.
        Putchar:        macro
                        xref    __putc
                        move.l  d0,-(sp)
                ifnc '\1',''
                        move.b  #'\1',d0
                endc
                        move.w  d0,-(sp)
                        jsr     __putc
                        addq.l  #2,a7
                        move.l  (sp)+,d0
                        endm

*       Transmit a binary byte via diag. port (in hex) for debugging
        Puthex:         macro
                        movem.l d0-d1,-(sp)
                        move.b  d0,d1
                        lsr.b   #4,d0
                        _to_hex
                        Putchar
                        move.b  d1,d0
                        and.b   #0fh,d0
                        _to_hex
                        Putchar
                        movem.l (sp)+,d0-d1
                        endm

        _crlf:          macro
                        move.l  d0,-(sp)
                        move.b  #13,d0
                        Putchar
                        move.b  #10,d0
                        Putchar
                        move.l  (sp)+,d0
                        endm
* End   :::::::::  Debugging Macros  ::::::::::

* Initialize a timer function
* usage:_init_tmr_fn <timer no.>,<function ptr>,<time>
        _init_tmr_fn:   macro
                        xref    _Init_Tmr_Fn
                        movem.l d0/a0,-(a7)
                        move.l  #\3,-(a7)
                        move.l  #\2,-(a7)
                        move.w  #\1,-(a7)
                        jsr     _Init_Tmr_Fn
                        adda.l  #10,a7
                        movem.l (a7)+,d0/a0
                        endm

* Load a timer
* usage:_load_tmr <timer no.>,<time>
        _load_tmr:      macro
                        xref    _Load_Tmr
                        movem.l d0,-(a7)
                        move.l  #\2,-(a7)
                        move.w  #\1,-(a7)
                        jsr     _Load_Tmr
                        addq.l  #6,a7
                        movem.l (a7)+,d0
                        endm

* Stop a timer
* usage:_stop_tmr <timer no.>
        _stop_tmr:      macro
                        xref    _Stop_Tmr
                        movem.l d0,-(a7)
                        move.w  #\1,-(a7)
                        jsr     _Stop_Tmr
                        addq.l  #2,a7
                        movem.l (a7)+,d0
                        endm

        list
