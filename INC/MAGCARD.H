/*----------------------------------------------------------------------------\
|  Src File:   magcard.h                                                      |
|  Authored:   09/20/94, ???                                                  |
|  Function:   Defines & prototypes for card processing.                      |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   09/20/94, tgh  -  Initial release.                             |
|      1.01:   11/14/98, sjb  -  added embedded transfer error flags.         |
|      1.02:   11/14/98, sjb  -  add "T DISABL","T UNLIST", & "BAD XFER"      |
|      1.03:   04/04/02, sjb  -  add "SHORT $" for Smart card validity        |
|                                                                             |
|           Copyright (c) 1993, 1994 GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
#ifndef  _MAGCARD_H
#define  _MAGCARD_H

/* Defines
*/


/*
** card validity flags
*/
#define PBK    0x00000001            /* passback detected                  */
#define AGLE   0x00000002            /* agency good list error             */
#define SCV    0x00000004            /* security code violation            */
#define BAD    0x00000008            /* badlisted or badlist bit set       */
#define MLE    0x00000010            /* media list error                   */
#define PED    0x00000020            /* pass entry disabled                */
#define NVY    0x00000040            /* not valid yet (start date)         */
#define EXP    0x00000080            /* expired (stop offset)              */
#define PK     0x00000100            /* non peak pass used in peak         */
#define OPK    0x00000200            /* non offpeak pass used in offpeak   */
#define WKD    0x00000400            /* non weekday pass used in weekday   */
#define SAT    0x00000800            /* non saturday pass used on saturday */
#define SUN    0x00001000            /* non sunday pass used on sunday     */
#define HOL    0x00002000            /* non holiday pass used on holiday   */
#define NRR    0x00004000            /* no remaining rides                 */
#define NRV    0x00008000            /* no remaining value                 */

#define TPB    0x00010000            /* transfer passback                  */
#define TAG    0x00020000            /* transfer agency goodlist error     */
#define TCE    0x00040000            /* transfer control table error       */
#define TEX    0x00080000            /* transfer expired                   */
#define TODSR  0x00100000            /* xfer opposite dir same rte error   */
#define TSDSR  0x00200000            /* xfer same     dir same rte error   */
#define TODDR  0x00400000            /* xfer opposite dir diff rte error   */
#define TSDDR  0x00800000            /* xfer same     dir diff rte error   */
#define TNT    0x01000000            /* transfer no trips left             */
#define TDIS   0x02000000            /* embedded xfer fare disabled v1.01  */
#define TMLE   0x04000000            /* embedded xfer media list err v1.01 */
#define BXFER  0x08000000            /* bad LA xfer xfer v1.02             */
#define SHORTM 0x10000000            /* short money on smart card          */

extern   int    Readout;

/*
** Card structure (superset of all card formats)
*/
typedef  struct
{
  byte   Format;                /* card's original format code           */
  byte   Group;                 /* card (or media) group                 */
  byte   Desig;                 /* card (or media) designation           */
  byte   Security;              /* card's security code                  */
  byte   MfgID;                 /* Manufacturer's ID code                */
  byte   FirstUse;              /* first use of rolling start date pass  */
  word   AgencyID;              /* card's issuing Agency                 */
  ulong  SeqNo;                 /* card's sequence number                */

  ulong  AbsExp;                /* card's absolute expiration (unix time)*/
  ulong  StartDate;             /* card's start date (unix time)         */
  ulong  Expiration;            /* card's normal expiration (unix time)  */
  ulong  XferExp;               /* transfer's expiration (unix time)     */
  word   ExpOff;                /* card epiration offset-from start date */
  word   TypeExp;               /* period pass expiration type           */

  byte   PrintPos;              /* card's next available print line      */
  byte   BadFlag;               /* card found on bad list                */
  word   RemVal;                /* card's remaining value                */
  word   TPBCode;               /* third party billing code              */
  word   IUFare;                /* transfer's inital use fare            */
  ulong  EmpID;                 /* employee's ID number                  */
  word   Usage;                 /* number of times card used             */

  byte   XferDesig;             /* transfer's designation                */
  byte   XferTrips;             /* number of trips used on this transfer */
  byte   IUDir;                 /* transfer's initial direction          */
  byte   Pad2;                  /* pad for word alignment                */
  word   IUAgency;              /* transfer's issuing agency             */
  ulong  IURoute;               /* transfer's issuing route              */
  ulong  IUBus;                 /* transfer's issuing bus                */
  word   XferExpOff;            /* transfer's expiration offset code     */
  word   LuVal;                 /* mag value used from card (LA)         */
  byte   GFISpare;              /* old GFI format card (spare 1)         */
  byte   Zone;                  /* old GFI format card zone (spare 2)    */
  byte   t_code;                /* LA t_code                             */
  byte   lu_day;                /* LA transfer expiration day (past 12am)*/
  word   lu_date;               /* LA transfer expiration day (past 12am)*/
  byte   LA_seq_no;             /*                                       */
  byte   spare1;                /*                                     */
  byte   serial[10];            /* serial number saved in passback & trans*/
  word   XferVal;               /* xfer value                             */
  byte   xflags;                /* bit mapped flags for xfer     */
  byte   embedxfer;             /* embedded xfer flag            */
  byte   KTFare;                /* fare paid for  used in transaction  */
  byte   spare2;                /*                                     */
  int    TTP_used;              /* ttp index to media list used in transaction  */
  int    TCIndex;
  word   FareDeducted;
  word   FareCell;
  byte   cc_serial[20];         /* serial number saved in transaction  */
} CARD;


/*
**  Serial number structure of format 0 cards
*/
typedef struct
{
  word   spare       :16;        
  ulong  Group       : 3;       /* card's group                          */
  ulong  Desig       : 5;       /* card's designation                    */
  ulong  SeqNo       :24;       /* sequence number                       */
  ulong  Security    : 4;       /* security code                         */
  ulong  AgencyID    :12;       /* issuing agency                        */
  ulong  MfgID       : 4;       /* manufaturer's ID code                 */
  ulong  TPBCode     :12;       /* third party billing code              */
} SERIAL_F0;


/*
**  Serial number structure of format 1 cards
*/
typedef struct
{
  word   spare       :16;        
  byte   Group       : 3;       /* card's group                          */
  byte   Desig       : 5;       /* card's designation                    */
  ulong  AgencyID    :12;       /* issuing agency                        */
  ulong  BusH        :12;       /* issuing bus number high               */
  ulong  BusL        : 8;       /* issuing bus number low                */
  ulong  SeqNo       :24;       /* sequence number ((24bits) issue_time) */
} SERIAL_F1;


/*
**  Serial number structure of format 2 cards
*/
typedef struct
{
  word   spare       :16;        
  ulong  Group       : 3;       /* card's group                          */
  ulong  Desig       : 5;       /* card's designation                    */
  ulong  EmpID       :24;       /* employee ID number                    */
  ulong  Security    : 4;       /* security code                         */
  ulong  AgencyID    :12;       /* issuing agency                        */
  ulong  MfgID       : 4;       /* manufaturer's ID code                 */
  ulong  SeqNo       :12;       /* sequence number                       */
} SERIAL_F2;

/*
**  Serial number structure of format 3 cards
*/
typedef struct
{
  word   spare       :16;        
  byte   Group       : 3;       /* card's group                          */
  byte   Desig       : 5;       /* fare type                             */
  ulong  SeqNo       :32;       /* sequence number                       */
  ulong  AgencyID    :12;       /* issuing agency                        */
  ulong  TPBCode     :12;       /* third party billing code              */
} SERIAL_F3;


/*
**  Serial number structure of format 1 cards
*/
typedef struct
{
  word   spare       :16;        
  ulong  Group       : 3;       /* card's group                          */
  ulong  Desig       : 5;       /* card's designation                    */
  ulong  SeqNo       :24;       /* sequence number                       */
  word   BusH        : 4;       /* issuing bus number high               */
  word   AgencyID    :12;       /* issuing agency                        */
  word   BusL        :16;       /* issuing bus number low                */
} SERIAL_F5;


/*
**  Serial number structure of format 3 cards
*/
typedef struct
{
  word   spare       :16;        
  byte   Group       : 3;       /* card's group                          */
  byte   Desig       : 5;       /* fare type                             */
  word   Security    : 4;       /* security code                         */
  word   AgencyID    :12;       /* issuing agency                        */
  byte   SeqNo[5];              /* sequence no.                          */
} SERIAL_F6;

/*
**  Serial number structure of format 0 cards
*/
typedef struct
{
  word   spare2      :16;        
  ulong  Group       : 3;       /* card's group                          */
  ulong  Desig       : 5;       /* card's designation                    */
  ulong  SeqNoL      :24;       /* sequence number                       */
  word   Security    : 4;       /* security code                         */
  word   AgencyID    :12;       /* issuing agency                        */
  word   spare       : 8;
  word   SeqNoH      : 8;       /* sequence number high                  */
} SERIAL_F7;

/*
**  Serial number structure of credit card format type cards
*/
typedef struct
{
  byte   SeqNo[10];             /* sequence no.                          */
} SERIAL_F8;                     

extern   CARD   TrimCard;
extern   CARD   SW_Card;

/* #define PROTOTYPE */

void CardProcessing( void );
void Init_Card_Irq1( void );
void Card_Clear( void );
int  SendReceipt( word, char*, int );

#endif /*_MAGCARD_H*/
