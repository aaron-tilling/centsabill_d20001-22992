/*----------------------------------------------------------------------------\
|  Src File:   magcon.h                                                       |
|  Authored:   09/20/94, ???                                                  |
|  Function:   Defines & prototypes for                                       |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   03/22/95, tgh  -  Initial release.                             |
|      1.01:   07/27/99, sjb  -  trim issued day pass.                        |
|                             -  2nd cut time for Santa Monica only           |
|           Copyright (c) 1994, 1995 GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
#ifndef  _MAGCON_H
#define  _MAGCON_H

/* Defines
*/
#define  _RTM       0x1000       /* xfer exp relative to midnight bit flag*/
#define  _CUTTM     0x1400       /* xfer exp cut time from farebox        */
#define  _CUTTM2    0x1800       /* xfer exp cut time from farebox        */
#define  _1MRES     0x0000       /* xfer exp 1 minute resolution          */
#define  _5MRES     0x0400       /* xfer exp 5 minute resolution          */
#define  _1HRES     0x0800       /* xfer exp 1 hour   resolution          */
#define  _1DRES     0x0c00       /* xfer exp 1 day    resolution          */
#define  _ALLRES    0x0c00       /* all xfer exp resolution flags         */
#define  _ISSUEDDAY 0x1c00       /* TRiM issued day pass                  */
#define  _EXPTIME   0x03ff       /* xfer exp time offset                  */

#define  _MIDNIGHT       0       /* expire at midnight                    */
#define  _END_TD         1       /* expire at end of transit day          */
#define  _HOUR24         2       /* expire 24 hours after being used      */
#define  _ADD_OFFSET     3       /* additional time after midnight        */


/* Prototypes
*/
int      GetCard           ( CARD *card, void *serial );
int      GetOldCard        ( byte *v, CARD *card, void *serial );
int      PutCard           ( CARD *card );
void     RegToCard         ( CARD *card );
void     XferToCard        ( CARD *card );
void     MaintToCard       ( CARD *card );
void     SPerToCard        ( CARD *card );
void     RegToSerial       ( void *serial );
void     XferToSerial      ( void *serial );
void     MaintToSerial     ( void *serial );
void     SPerToSerial      ( void *serial );
void     CardToReg         ( VDB0 *v, CARD *card );
void     CardToXfer        ( VDB1 *v, CARD *card );
void     CardToMaint       ( VDB2 *v, CARD *card );
void     CardToSPer        ( VDB3 *v, CARD *card );
time_t   ComputeXferExp    ( word expire );


#endif /*_MAGCON_H*/
