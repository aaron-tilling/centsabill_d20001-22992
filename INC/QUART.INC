*-----------------------------------------------------------------------------+
*  Src File:   quart.inc                                                      |
*  Authored:   01/11/96, tgh                                                  |
*  Function:   QUART equates for new farebox logic board.                     |
*  Comments:   This is a mix of DUART and QUART equates time could be taken   |
*              to remove redundant equates.                                   |
*                                                                             |
*  Revision:                                                                  |
*      1.00:   01/11/96, tgh  -  Initial release.                             |
*                                                                             |
*           Copyright (c) 1993-1996  GFI All Rights Reserved                  |
*-----------------------------------------------------------------------------+

        quart0base:     equ     0200000h


        mra:            equ     000h    mode register A                 (r/w)
        sra:            equ     001h    status register A               (r)
        csra:           equ     001h    clock select register           (w)
        cra:            equ     002h    command register A              (w)
        rhra:           equ     003h    recieve hold register A         (r)
        thra:           equ     003h    transmit hold register A        (w)

        ipcr:           equ     004h    input port change register      (r)
*        acr:            equ     004h    auxiliary control register      (w)
*        isr:            equ     005h    interupt status register        (r)
*        imr:            equ     005h    interupt mask register          (w)
        ctu:            equ     006h    counter/timer upper             (r)
        ctur:           equ     006h    counter/timer upper register    (w)
        ctl:            equ     007h    counter/timer lower             (r)
        ctlr:           equ     007h    counter/timer lower register    (w)

        mrb:            equ     008h    mode register B                 (r/w)
        srb:            equ     009h    status register B               (r)
        csrb:           equ     009h    clock select register           (w)
        crb:            equ     00ah    command register B              (w)
        rhrb:           equ     00bh    recieve hold register B         (r)
        thrb:           equ     00bh    transmit hold register B        (w)

        ipr:            equ     00dh    input port register             (r)
        opcr:           equ     00dh    output port config register     (w)
        start_cnt:      equ     00eh    start counter command           (r)
        sopb:           equ     00eh    set output port bits command    (w)
        stop_cnt:       equ     00fh    stop counter command            (r)
        ropb:           equ     00fh    reset output port bits command  (w)

        mrc:            equ     010h    mode register C                 (r/w)
        src:            equ     011h    status register C               (r)
        csrc:           equ     011h    clock select register C         (w)
        crc:            equ     012h    command register C              (w)
        rhrc:           equ     013h    recieve holding register C      (r)
        thrc:           equ     013h    transmit holding register C     (w)

        mrd:            equ     018h    mode register D                 (r/w)
        srd:            equ     019h    status register D               (r)
        csrd:           equ     019h    clock select register D         (w)
        crd:            equ     01ah    comand register D               (w)
        rhrd:           equ     01bh    recieve holding register D      (w)
        thrd:           equ     01bh    transmit holding register D     (w)

        misra:          equ     002h    Masked Interupt Status Register A
        ipcr1:          equ     004h    Input Port Change Regester 1
        acra:           equ     004h    Auxiliary Control Register A
        isr1:           equ     005h    Interupt Status Register 1
        ctu1:           equ     006h    Counter/Timer 1 Upper Byte
        ctl1:           equ     007h    Counter/Timer 1 Lower byte
        imr1:           equ     005h    Interupt Mask Register 1
        ivr1:           equ     00ch    Interupt Vector register 1
        ip1:            equ     00dh    Input Port 1
        opcra:          equ     00dh    Output Port config reg. A
        scc1:           equ     00eh    Start Counter/Timer 1
        sopbc1:         equ     00eh    Set Output Port bits Command 1
        stc1:           equ     00fh    Stop Counter/Timer Commad 1
        ropbc1:         equ     00fh    Reset Output Port Bits Command 1
        
        misrb:          equ     012h    Masked Interupt Status Register B
        ipcr2:          equ     014h    Input Port Change Register 2
        acrb:           equ     014h    Auxiliary Control Register B
        isr2:           equ     015h    Interupt Status Register 2
        imr2:           equ     015h    Interupt Mask Register 2
        ctu2:           equ     016h    Counter/Timer 2 Upper Byte
        ctl2:           equ     017h    Counter/Timer 2 Lower Byte
        ivr2:           equ     01ch    Interupt Vector register 2
        ip2:            equ     01dh    Input Port 2
        opcrb:          equ     01dh    Output Port Config Reg. B
        scc2:           equ     01eh    Start Counter/Timer 2
        sopbc2:         equ     01eh    Set Output Port Bits Command 2
        stc2:           equ     01fh    Stop Counter/Timer Command 2
        ropbc2:         equ     01fh    reset Output Port bits Command 2

