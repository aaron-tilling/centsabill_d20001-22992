/*----------------------------------------------------------------------------\
|  Src File:   comm.h                                                         |
|  Authored:   01/11/96, tgh                                                  |
|  Function:   Communication process defines and macros for C modules.        |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   01/11/97, tgh  -  Initial release.                             |
|                                                                             |
|           Copyright (c) 1993-1997  GFI/USPS All Rights Reserved             |
\----------------------------------------------------------------------------*/
#ifndef  _COMM_H
#define  _COMM_H

/* Defines
*/
#define  COMM_232             0x0001            /* RS232 connector J1 pin 3,4*/
#define  COMM_DBG             0x0002            /* diagnostic (stdin/stdout) */
#define  COMM_485             0x0004            /* RS485 TRIM communications */
#define  COMM_PRB             0x0008            /* probe commnications       */
#define  COMM_SCI             0x0010            /* SCI (1708) J1 pin 17,18   */


/* Prototypes
*/
int   FlagCommRx( unsigned int );

#endif /* _COMM_H */
