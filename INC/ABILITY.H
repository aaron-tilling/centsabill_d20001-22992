/*----------------------------------------------------------------------------\
|  Src File:   ability.h                                                      |
|  Authored:   03/15/95, tgh                                                  |
|  Function:   Defines & macros for ability flags.                            |
|  Comments:   "Aflags" are allocated in "main_vxx.c".                        |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   03/15/95, tgh  -  Initial release.                             |
|      1.01:   11/22/96, sjb  -  change ability flag defines to unsigned longs|
|      1.02:   02/17/99, sjb  -  add flag to display BLOCK in place of RUN    |
|                                                                             |
|           Copyright (c) 1994, 1995 GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
#ifndef  _ABILITY_H
#define  _ABILITY_H

/*----------------------------------------------------------------------------\
|                         ABILITY FLAGS & MACROS                              |
\----------------------------------------------------------------------------*/

#define  A_MAKECARDS     (ulong)0x00000001     /* can make cards from menu  */
#define  A_CUTTIME       (ulong)0x00000002     /* cut time used for trnsfrs.*/
#define  A_DIRECTION     (ulong)0x00000004     /* N,S,E,W used(not inb/outb)*/
#define  A_LOWPOWER      (ulong)0x00000008     /* don't process card iflow power */
#define  A_CANADA        (ulong)0x00000010     /* Canada bill & coins        */
#define  A_HOUR_RR       (ulong)0x00000020     /* store hourly r/r           */
#define  A_HALFHOUR_RR   (ulong)0x00000040     /* store � hour r/r           */
#define  A_QUARHOUR_RR   (ulong)0x00000080     /* store � hour r/r           */
#define  A_JAMAICA       (ulong)0x00000100     /* Jamiaca coins              */
#define  A_TRIP          (ulong)0x00000200     /* TRIP used (not DIRECT)     */
#define  A_SPANISH       (ulong)0x00001000     /* Spanish prompts            */
#define  A_BIG_FARES     (ulong)0x00002000     /* large fare values          */
#define  A_J1708         (ulong)0x00004000     /* j1708                      */
#define  A_PCCARD_CONFIG (ulong)0x00008000     /* Configured via PCMCIA Card */
#define  A_POWER         (ulong)0x00010000
#define  A_P_DETECT      (ulong)0x00020000

#define  ALL_AFLAGS     (unsigned long)\
                        (A_MAKECARDS|A_CUTTIME|A_DIRECTION|A_LOWPOWER|\
                         A_CANADA|A_HOUR_RR|A_HALFHOUR_RR|A_QUARHOUR_RR|\
                         A_JAMAICA|A_TRIP|A_SPANISH|A_BIG_FARES|A_J1708|\
                         A_PCCARD_CONFIG|A_POWER|A_P_DETECT)

extern unsigned long  Aflags;
#define  Set_Aflag( f )          ( Aflags |= (f) )
#define  Clr_Aflag( f )          ( Aflags &=~(f) )
#define  Tst_Aflag( f )          ( Aflags &  (f) )

#endif /*_ABILITY_H*/
