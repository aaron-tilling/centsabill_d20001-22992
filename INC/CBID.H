/*----------------------------------------------------------------------------\
|  Src File:   cbid.h                                                         |
|  Authored:   03/04/96, sjb                                                  |
|  Function:   Defines & macros for processing cashbox.                         |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   03/04/96, sjb  -  Initial release.                             |
|      1.01:   07/30/97, sjb  -  add Cashbox_In.                              |
|                                                                             |
|           Copyright (c) 1994 - 1997 GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
#ifndef  _CBID_H
#define  _CBID_H

/* Prototype(s)
*/
void CashboxFault   ( void );
void Cashbox_Out_Ck ( void );
void Cashbox_In     ( void );

#endif /*_CBID_H*/
