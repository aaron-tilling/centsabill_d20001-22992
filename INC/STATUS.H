/*----------------------------------------------------------------------------\
|  Src File:   status.h                                                       |
|  Authored:   11/28/94, tgh                                                  |
|  Function:   Defines & macros for status flags.                             |
|  Comments:   "Sflags" are allocated in "stat_vxx.c".                        |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   11/28/94, tgh  -  Initial release.                             |
|                                                                             |
|           Copyright (c) 1993, 1994 GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
#ifndef  _STATUS_H
#define  _STATUS_H

/*----------------------------------------------------------------------------\
|                          STATUS FLAGS & MACROS                              |
\----------------------------------------------------------------------------*/

#define  S_DUMP            (ulong)0x00000001    /* dump key active         */
#define  S_DOOR            (ulong)0x00000002    /* door switch active      */
#define  S_CTHRT           (ulong)0x00000004    /* coin throat blocked     */
#define  S_BILL            (ulong)0x00000008    /* bill sensor blocked     */
#define  S_BILL_T          (ulong)0x00000010    /* bill throat blocked     */
#define  S_BYPASS          (ulong)0x00000020    /* bypass switch active    */
#define  S_BURNIN          (ulong)0x00000040    /* burnin test jumper      */
#define  S_MEMCLR          (ulong)0x00000080    /* memory clear jumper     */
#define  S_SPARES1         (ulong)0x00000100    /* spare 1 jumper          */
#define  S_SPARES2         (ulong)0x00000200    /* spare 2 jumper          */
#define  S_SPARES3         (ulong)0x00000400    /* spare 3 jumper          */
#define  S_CASHBOX         (ulong)0x00000800    /* cashbox present timer   */
#define  S_PROBING         (ulong)0x00001000    /* currently probing       */
#define  S_COMPLETEPENDING (ulong)0x00002000    /* probe just completed    */
#define  S_FULLCASHBOX     (ulong)0x00004000    /* full cashbox            */
#define  S_BYPASS_P        (ulong)0x00008000    /* bypass switch was active*/
#define  S_INVALIDCNF      (ulong)0x00010000    /* invalid configuration   */
#define  S_SELFTEST        (ulong)0x00020000    /* farebox in self test    */
#define  S_TRIMOFFLINE     (ulong)0x00040000    /* comm. offline           */
#define  S_INVALIDFS       (ulong)0x00080000    /* invalid fare structure  */
#define  S_PROCESSING      (ulong)0x00100000    /* processing a trim card  */
#define  S_CARDPRESENT     (ulong)0x00200000    /* card in escrow          */
#define  S_TESTSET         (ulong)0x00400000    /* test set flag           */
#define  S_RS232           (ulong)0x00800000    /* rs232 test              */
/*
#define  S_RAMCARD         (ulong)0x01000000    /* ram card test           */
#define  S_FAREHOLD        (ulong)0x01000000    /*                         */
#define  S_NOSTOCK         (ulong)0x02000000    /* stock empty             */
#define  S_LOWSTOCK        (ulong)0x04000000    /* trim low stock          */
#define  S_ESCROW          (ulong)0x08000000    /* card held in escrow     */
#define  S_TRIMBYPASS      (ulong)0x10000000    /* trim in bypass          */
#define  S_TRIM_JAM        (ulong)0x20000000    /* trim card jam detected  */
#define  S_PROBED          (ulong)0x40000000    /* probed with alarm delay */
#define  S_CREDITCARD      (ulong)0x80000000    /* credit card in process  */

#define  ALL_SFLAGS     (unsigned long)\
                        (S_DUMP|S_DOOR|S_CTHRT|S_BILL|S_BILL_T|S_BYPASS|\
                         S_BURNIN|S_MEMCLR|S_SPARES1|S_SPARES2|S_SPARES3|\
                         S_CASHBOX|S_FULLCASHBOX|S_BYPASS_P|S_INVALIDCNF|\
                         S_SELFTEST|S_TRIMOFFLINE|S_INVALIDFS|S_PROCESSING|\
                         S_CARDPRESENT|S_TESTSET|S_RS232|S_FAREHOLD|S_NOSTOCK|\
                         S_LOWSTOCK|S_ESCROW|S_TRIMBYPASS|S_TRIM_JAM|S_PROBED|\
                         S_CREDITCARD)

/* If any of these flags are set we're
   out of service.
*/
#define  CRITICAL_FLAGS (unsigned long)\
                        (S_INVALIDCNF|S_DOOR|S_CASHBOX)

extern   unsigned long           Sflags;
extern   unsigned long           AutoDumpTime;
extern             int           BadBatteryDetect; /* bad battery detected */

#define  Set_Sflag( f )          ( Sflags |= (f) )
#define  Clr_Sflag( f )          ( Sflags &=~(f) )
#define  Tst_Sflag( f )          ( Sflags &  (f) )

/* Prototypes
*/
int   InitializeStatus  ( void );
void  CheckStatus       ( void );
void  CheckCashbox      ( void );
void  Init_CBID_Irq4    ( int t );
int   MachineOK         ( void );
void  ServiceTest       ( void );

#endif

