/*----------------------------------------------------------------------------\
|  Src File:   tests.h                                                        |
|  Authored:   09/20/94, ???                                                  |
|  Function:   Defines & prototypes for                                       |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   02/13/95, tgh  -  Initial release.                             |
|                             -  fixed return types.                          |
|      1.01:   07/27/99, sjb  -  increas serial no size to 10 byte to handle  |
|                                credit card formatted cards                  |
|                                                                             |
|           Copyright (c) 1993, 1994 GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
#ifndef  _TESTS_H
#define  _TESTS_H

#define  GOODPEAK       0x01                    /* attribute flags */
#define  GOODOFFPEAK    0x02
#define  GOODWKD        0x04
#define  GOODSAT        0x08
#define  GOODSUN        0x10
#define  GOODHOL        0x20
#define  GOODFRIENDLY   0x40
#define  DISABLED       0x80


int   isbadlisted             ( CARD *c, B_SERIAL *s );
ulong is_date_ok              ( CARD *p );
ulong is_sc_date_ok           ( CARD *p );
int   is_friendly_ok          ( CARD *p );
ulong check_card_restrictions ( int index );
ulong check_xfer_restrictions ( int index, CARD *card );
int   Upgrade                 ( int index );
#endif /*_TESTS_H */
