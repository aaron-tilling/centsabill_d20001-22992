/*----------------------------------------------------------------------------\
|  Src File:   lamp.h                                                         |
|  Authored:   03/04/96, sjb                                                  |
|  Function:   Defines & macros for escrow lamp                               |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   03/04/96, sjb  -  Initial release.                             |
|           Copyright (c) 1994 - 1996 GFI All Rights Reserved                 |
\----------------------------------------------------------------------------*/
#ifndef  _LAMP_H
#define  _LAMP_H

extern          int     Lamp_Delay;

/* #define PROTOTYPE */

void Lamp( void );
void EscrowLamp( void );

#endif /*_LAMP_H*/
