/*----------------------------------------------------------------------------\
|  Src File:   regs_v03.c                                                     |
|  Authored:   01/09/96, tgh                                                  |
|  Function:   Define variables to access hardware registers.                 |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   01/09/96, tgh  -  Initial release.                             |
|      1.02:   07/27/99, sjb  -  change interrupt level of gpt.               |
|      1.02:   06/11/01, sjb  -  setup a21 address line for pccard in portf.  |
|                                                                             |
|           Copyright (c) 1993, 1994 GFI/USPS All Rights Reserved             |
\----------------------------------------------------------------------------*/
/* System headers
*/
#include <stdio.h>

/* Project headers
*/
#include "regs.h"

#option sep_on segment SIM_regs
union _smcr     smcr;
union _simtr    simtr;
union _syncr    syncr;
union _rsr      rsr;
word  simtre;
word  unused_1[3];
union _porte_1  porte_1;
union _porte_2  porte_2;
union _ddre     ddre;
union _pepar    pepar;
union _portf_1  portf_1;
union _portf_2  portf_2;
union _ddrf     ddrf;
union _pfpar    pfpar;
union _sypcr    sypcr;
union _picr     picr;
union _pitr     pitr;
union _swsr     swsr;
word  unused_2[4];
word  tstmsra;                      /* 16 bit contiguous register */
word  tstmsrb;                      /* 16 bit contiguous register */
word  tstsc;                        /* 16 bit contiguous register */
word  tstrc;                        /* 16 bit contiguous register */
union _creg     creg;
union _dreg     dreg;
word  unused_3[2];
union _portc    portc;
word  unused_4;
union _cspar0   cspar0;
union _cspar1   cspar1;
union _csbarbt  csbarbt;
union _csorbt   csorbt;
union _csbar0   csbar0;
union _csor0    csor0;
union _csbar1   csbar1;
union _csor1    csor1;
union _csbar2   csbar2;
union _csor2    csor2;
union _csbar3   csbar3;
union _csor3    csor3;
union _csbar4   csbar4;
union _csor4    csor4;
union _csbar5   csbar5;
union _csor5    csor5;
union _csbar6   csbar6;
union _csor6    csor6;
union _csbar7   csbar7;
union _csor7    csor7;
union _csbar8   csbar8;
union _csor8    csor8;
union _csbar9   csbar9;
union _csor9    csor9;
union _csbar10  csbar10;
union _csor10   csor10;
#option sep_off



/*
*--------------------------------------------------------------------------
*                               QSM DECLARATIONS
*/
#option sep_on segment QSM_regs

union _qmcr     qmcr;
union _qtest    qtest;
union _qilr_qivr qilr_qivr;
word  qsm_rsvd_1;
union _sccr0    sccr0;
union _sccr1    sccr1;
union _scsr     scsr;
union _scdr     scdr;
word  qsm_rsvd_2;
word  qsm_rsvd_3;
union _qpdr     qpdr;
union _qpar_qddr qpar_qddr;
union _spcr0    spcr0;
union _spcr1    spcr1;
union _spcr2    spcr2;
union _spcr3_spsr spcr3_spsr;
#option sep_off


/*
*--------------------------------------------------------------------------
*                               QSM QUEUE RAM
*/
#option sep_on segment QSPI_qram

word rec_ram[16];
word tran_ram[16];
byte comd_ram[16];
#option sep_off


/*
*--------------------------------------------------------------------------
*                               GPT DECLARATIONS
*/
#option sep_on segment GPT_regs

union _gmcr     gmcr;
word  gpt_rsv_1;
union _icr icr;
union _pddr_pdr pddr_pdr;
union _oc1m_oc1d oc1m_oc1d;
word  tcnt;            /* 16 bit timer counter reg */
union _pactl_pacnt pactl_pacnt;
word  tic1;
word  tic2;
word  tic3;
word  toc1;
word  toc2;
word  toc3;
word  toc4;
word  ti4o5;
union _tctl1_tctl2 tctl1_tctl2;
union _tmsk1_tmsk2 tmsk1_tmsk2;
union _tflg1_tflg2 tflg1_tflg2;
union _cforc_pwmc  cforc_pwmc;
word  pwma_pwmb;
word  pwmcnt;
word  pwmab_buf;
word  prescaler;
word  pwm_rsvd;

#option sep_off


void  Initialize_Qsm()
{
   qmcr.b.iarb          = 13;                   /* supervisor - arb = 14 */
   qilr_qivr.b.ilqspi   = 6;                    /* qspi int level = 6    */
   qilr_qivr.b.ilsci    = 5;                    /* sci int level  = 5    */
   qilr_qivr.b.intv     = 0xec;                 /* vector base 0xec      */

   qpdr.w               = 0;                    /* negate pcs0 (cs) line */

   /* qpar_qddr: (txd,pcs0,sck,osi) = outputs   */
   /* qpar_qddr: (pcs0,mosi,miso) used for qspi */

   qpar_qddr.b.mosi     = 1;
   qpar_qddr.b.miso     = 1;

   qpar_qddr.b._txd     = 1;                    /* sci (1708) data transmit */
   qpar_qddr.b._pcs3    = 1;                    /* 1708 transmit en/dis able*/
   qpar_qddr.b._pcs0    = 1;
   qpar_qddr.b._sck     = 1;
   qpar_qddr.b._mosi    = 1;

   spcr0.w              = 0;                    /* Initially clear all   */
   spcr0.b.mstr         = 1;                    /* master mode           */
   spcr0.b.bits         = 8;                    /* # of data bits        */
   spcr0.b.baud         = 150;                  /* Produces baud 100kHZ  */


   spcr2.w              = 0;                    /* adjusted as needed    */

   spcr3_spsr.b.loopq   = 0;
   spcr3_spsr.b.hmie    = 0;
   spcr3_spsr.b.halt    = 0;

   spcr1.b.spe          = 0;                    /* disable qspi          */
   spcr1.b.dsclk        =17;                    /* 1uS delay before SCK  */
   spcr1.b.dtl          = 4;                    /* 7.6uS del after xfer  */
}


void  Initialize_Gpt()
{
   gmcr.w               = 0x008e;               /* supervisor - base vector */
   icr.w                = 0x01e0;               /* base vector ver 1.02     */
   tmsk1_tmsk2.b.cpr    = 2;                    /* compare/capture prescale */
}                                               /* divide by 16             */

void  Initialize_PortF()
{

   portf_1.w = 0;
   ddrf.w    = 0x0010;          /* setup as output i/o for pccard address a21 */ 
   pfpar.w   = 0x0008;          /* port-f pin assignments */
                                /* irq1=card              */
                                /* irq2=key               */
                                /* irq3=QUART             */
}
