/*----------------------------------------------------------------------------\
|  Src File:   stat_v41.c                                                     |
|  Authored:   12/02/94, tgh                                                  |
|  Function:   Check and Maintain machine status flags.                       |
|  Comments:   Macros used to maintain "Status" are in "status.h".            |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   12/14/94, tgh  -  Initial release.                             |
|      1.01:   01/23/97, sjb  -                                               |
|      1.02:   01/23/97, sjb  -  modify walking decimal point                 |
|      1.03:   03/13/97, sjb  -  limit bus & fb no.                           |
|      1.04:   08/27/97, sjb  -  check for pc card to reprogram flash         |
|      1.05:   10/14/97, sjb  -  send configuration to PPS when notify flags  |
|                                change                                       | 
|      1.06:   11/07/97, sjb  -  check if ver 4  if time to be displayed when |
|                                idle                                         |
|      1.07:   11/07/97, sjb  -  display time in 12 hour format for CATA      |
|      1.08:   01/28/98, sjb  -  modify time display for CATA                 |
|      1.09:   02/11/98, sjb  -  modify service routine                       |
|      1.10:   04/02/98, sjb  -  modify service routine                       |
|      1.11:   05/12/98, sjb  -  clear any pending xfer to be issed via xfer  |
|                                key (*11)                                    |
|      1.12:   10/20/98, sjb  -  check for cut time                           |
|      1.13:   10/20/98, sjb  -  modify check for cut time                    |
|      1.14:   11/19/98, aat  -  change cut time default to **:**             |
|      1.15:   02/02/99, aat  -  move Driver_Rev, CashboxBill, & CashboxCoin  |
|                                to this moduel                               |
|                             -  initalize S_PROCESSING,S_CARDPRESENT, &      |
|                                S_ESCROW flags in InitializeStatus           | 
|      1.16:   05/07/99, sjb  -  modify messages sent to 2nd display of DCU   |
|      1.17:   07/01/99, nsr  -  BCTRANSIT Enhancements                       |
|      1.18:   07/01/99, sjb  -  BCTRANSIT Enhancements                       |
|      1.19:   10/12/99, sjb  -  test code to use farebox as coin mench tester|
|                                (see  NOTE: )                             |
|      1.20:   12/08/99, sjb  -  modify out of service messages               |
|      1.24:   06/14/00, sjb  -  request TRiM dianostic every minute          |
|      1.25:   09/01/00, aat  -  added text for NO BLOCK if block is used.    |
|      1.26:   09/11/00, SJB  -  MODIFIED FOR J1708 requires to remove PPS    |
|                             -  coin & bill full threshold received from ds  |
|      1.27:   01/05/01, aat  -  modify battery checking for 20750.           |
|      1.28:   03/01/01, aat  -  clear P_RECHARGE flag on powerup.            |
|      1.29:   12/01/01, sjb  -  when checking driver login status, if Burnin |
|                                jumper in place, ignore invalid confgiuration|
|      1.30:   02/21/02, sjb  -  add hooks to talk to DCU on 485              |
|      1.31:   03/07/02, sjb  -  TRIP/BLOCK display                           |
|      1.32:   05/13/02, sjb  -  modify full cashbox stuff for j1708          |
|                        sjb  -  increase driver no to 7 digits to handle old |
|                                gfi swipe cards                              |
|      1.32:   08/29/02, sjb  -  modify full cashbox stuff for j1708          |
|      1.33:   09/02/02, sjb  -  modify MachineOK to send message on J1708    |
|      1.33:   09/02/02, sjb  -  modify processing battery & badclock flags   |
|      1.35:   05/20/03, sjb  -  modify processing PPS messages               |
|      1.36:   06/10/03, sjb  -  modify to process PID 502                    |
|                             - don't clear PROCESSING/CARDPRESENT/ESCROW     |
|                               flag in InitializeStatus                      |
|      1.37:  10/23/03, sjb  -  don't send/request trim diagnostics or status |
|                               when in card is being processed               |
|      1.38:  08/13/04, sjb  -  modify display if trim in bypass or jammed    |
|      1.39:  10/20/04, sjb  -  correct display of TRIM OFFLINE               |
|      1.40:  10/26/04, sjb  -  correct display of TRIM OFFLINE               |
|      1.41:  01/18/05, sjb  -  send "UNLOCKED" & "CASHBOX" messages to TRiM  |
|                               display                                       |
|           Copyright (c) 1993-2001  GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
/* System headers
*/
#include <stdio.h>

/* Project headers
*/
#include "cnf.h"
#include "sci.h"
#include "regs.h"
#include "status.h"
#include "ability.h"
#include "timers.h"
#include "transact.h"
#include "fbd.h"
#include "pps.h"
#include "diag.h"
#include "menu.h"
#include "display.h"
#include "door.h"
#include "port.h"
#include "trim.h"
#include "pccard.h"
#include "misc.h"
#include "rtc.h"
#include "fare.h"
#include "dll.h"
#include "bill.h"
#include "process.h"
#include "j1708.h"
#include "sclk.h"

/* Define(s)
*/
#define  STATUS_CHK_TIME        MSEC( 250 )  /* how often to check status*/
#define  Seconds( n )           ( (n) * 4 )  /* 4 x STATUS_CHK_TIME */
#define  CNF_CHK_TIME          Seconds( 1 )
#define  RAMCARD_TIME          Seconds( 3 )
#define  TRIMSTATUS_TIME      Seconds( 60 )
#define  TRIM_TIME           Seconds( 120 )

/* If any of these flags are set we're
   out of service.

#define  CRITICAL_FLAGS (unsigned long)\
                        (S_INVALIDCNF|S_DOOR|S_CASHBOX)
*/

/* If any of these flags change we'll
   notify the farebox with a status message.
*/
#define  NOTIFY_FLAGS   (unsigned long)\
                        (S_INVALIDCNF|S_DOOR|S_CASHBOX)

/* Global Data
*/
static   unsigned long        Signiture1;

unsigned long  Sflags;              /* Status flags */
word           Driver_Rev;
word           CashboxBill;         /* paper in cashbox              */
long           CashboxCoin;         /* coin revenue in cashbox  */
ulong          CashboxRevenue; 
int            CoinFullFlag;
int            BillFullFlag;
int            BadBatteryDetect;      /* bad battery detected  */
int            BadClock;
static   unsigned long        Signiture2;

unsigned long                 AutoDumpTime;

/* Module data
*/
static   int                  BatteryCheck;

/* Prototype(s)
*/

/*----------------------------------------------\
|  Function:   Initialize_Status();             |
|  Purpose :   Initialize status flags (if need)|
|  Synopsis:   int   Initialize_Status( void ); |
|  Input   :   None.                            |
|  Output  :   None.                            |
|  Comments:                                    |
\----------------------------------------------*/
int   InitializeStatus()
{
   int    E = 0;

   if ( Signiture1 != 0xfeedf00d || Signiture2 != 0xfeedf00d )
   {
      Signiture1 = Signiture2 = 0xfeedf00d;
      Sflags = 0;
      BadClock    = BadBatteryDetect = 0; 
      Driver_Rev  = 0;
      CashboxBill = 0;
      CashboxCoin = CashboxRevenue = 0L;
      CoinFullFlag = BillFullFlag = 0;
      Bill_Transport_Enabled = TRUE;
      E = TRUE;       /* flag error for retrun */      
   }
   else
      Sflags &= ALL_SFLAGS;

   Pflags &= ALL_PFLAGS;
   Aflags &= ALL_AFLAGS;

   BatteryCheck = TRUE; 
   BattVd_Set(BATT_CHECK);  /* get ready to check battery must be after Rtc */
   Init_Tmr_Fn( T_STATUS_CHK, CheckStatus, STATUS_CHK_TIME );

   Init_Tmr_Fn( PPSTMR, PPSPoll, SECONDS( 1 ) );

   Init_Tmr_Fn( SRVC_TEST_TMR, ServiceTest, SECONDS( 1 ) ); 
/*  Clr_Sflag( S_PROCESSING|S_CARDPRESENT|S_ESCROW );  /* clear flags */
   Clr_Pflag( P_RECHARGE );
   return E;
}

/*----------------------------------------------\
|  Function:   MachineOK();                     |
|  Purpose :   See if machine can be in service.|
|  Synopsis:   int   MachineOK( void );         |
|  Input   :   None.                            |
|  Output  :   None.                            |
|  Comments:                                    |
\----------------------------------------------*/
int   MachineOK()
{
static  int   Last;
        int   E = 0;

   /* If any one of these are set where out of service */
   if( !tmr_expired( DATA_ENTRY_TMR ) )
   {
      CheckEntry();
      CheckCnf();
   }

  /*  NOTE: S_SPARES1 tested for coin test to keep farebox in operation */
   if ( Tst_Sflag( CRITICAL_FLAGS ) && !Tst_Sflag( S_BURNIN|S_SPARES1 ) )
   {
      Mode = OUT_OF_SERVICE;
      Xfer_Key = MAX_TC << 12;
   }
   else
      E = 1;        /* Passed all tests so set flag */

   if ( Last != E )
   {
      Last = E;
/*
      if ( E )
      {
         GPSSendStatus( IN_SERVICE_STAT ); 
      }
      else
      {
         GPSSendStatus( OUT_SER_STAT );
      }
*/
   }

   return E;
}


/*----------------------------------------------\
|  Function:   CheckStatus();                   |
|  Purpose :   Check machine status             |
|  Synopsis:   void  CheckStatus( void );       |
|  Input   :   None.                            |
|  Output  :   None.                            |
|  Comments:                                    |
\----------------------------------------------*/
void  CheckStatus()
{
   /* initialize timers with staggered starting time
   */
   static         CnfTimer       =  CNF_CHK_TIME;
/* static         RamCardTimer   =  RAMCARD_TIME; */
   static         TrimStatusTmr  =  Seconds(1);
   static         TrimTimeTmr    =  Seconds(2);

   static         ulong  lastflags;
    
   if( BatteryCheck )                           /* check battery ? */
   {
      BatteryCheck = FALSE;
      if ( (LogicBoard != 20750 && !BATT_STATUS )||
        (LogicBoard == 20750 && !BATTRY_STATUS ) )
      {
         if ( BadBatteryDetect != CLOCK_ERROR )
         {
            NewEv( Ev_BAD_CLOCK, 0 );              /* battery bad */
            BadBatteryDetect = CLOCK_ERROR; 
         }
      }
      BattVd_Clear(BATT_CHECK);                 /* make sure line off */
   }

   if ( lastflags != (Sflags & NOTIFY_FLAGS) )  /* check for change */
   {
      lastflags = (Sflags & NOTIFY_FLAGS);
      Init_Tmr_Fn( SRVC_TEST_TMR, ServiceTest, 1 ); 
      if( !Tst_Sflag( S_TRIMOFFLINE|S_PROBING )  )  /* trim on line ? */
         SendStatus();

      PPSSendFbxCnf();
      TrimStatusTmr = TRIMSTATUS_TIME;
   }

   /* Reload here since we may not reach
      the end of the function
   */
   Init_Tmr_Fn( T_STATUS_CHK, CheckStatus, STATUS_CHK_TIME );

   if ( --CnfTimer <= 0 )
   {
      if( tmr_expired( DATA_ENTRY_TMR ) )
         CheckCnf();                      /* check configuration */
      CnfTimer = CNF_CHK_TIME;
   }

/**
   if ( --RamCardTimer <= 0 )
   {
      if ( Tst_Sflag( S_TESTSET ) )      
      {
         TestRS232();                    
         if ( (portj.w & 0x06) || CheckRamCard() )
            Set_Sflag( S_RAMCARD );       
         else
            Clr_Sflag( S_RAMCARD );       
      }
      RamCardTimer = RAMCARD_TIME;
   }
**/

   if ( --TrimStatusTmr <= 0 )
   {
      if( !Tst_Sflag( S_TRIMOFFLINE|S_PROBING )  )   /* trim on line ? */
      {
         SendStatus();                /* sent fb status to trim every min */
         TrimCmd( TRIMREQSTATUS, NULL, 0 );
      }
      TrimStatusTmr = TRIMSTATUS_TIME;
   }

   if ( --TrimTimeTmr <= 0 )         
   {
      if( !Tst_Sflag( S_TRIMOFFLINE|S_PROCESSING|S_PROBING|S_SELFTEST|S_BURNIN ) )
      {
         SendTime();                          /* sent time to trim */
         TrimCmd( TRIM_DIAG_REC, NULL, 0 );
      }
      TrimTimeTmr = TRIM_TIME;
   }

   if ( !Tst_Sflag( S_BURNIN ) )           /* test jumper in place ? */ 
   {
      for(;;)
      {
         if ( portj.w & 0x06 )             /* is card present  ? */      
            break;
         if ( PCcard.id != PCMCIA_PGMID )  /* program pc card ?  */    
            break;
      }      
   }

   return;
}

int   CheckCnf()
{
   int     E = 0, i;
   time_t      t;   
   union
   {
     FARE      p;
     word      w;
   }           u;

   /* Check ranges here
   */

   if ( (AutoDumpTime = SECONDS(Clist.PRIVATE.AutoDumpTmo)) <  SECONDS( 10 ) )
       AutoDumpTime = SECONDS( 10 );

   if ( Clist.PRIVATE.PassBackDuration == 0 )         /* cut-time minutes */
      Clist.PRIVATE.PassBackDuration = 1;

   /* These we need to be in service
   */

   if ( FbxCnf.driver <= 0L || 9999999L < FbxCnf.driver )
   {
      FbxCnf.driver  = 0L;
      if( NO_DRIVER )
         E = -1;
   }
   if ( FbxCnf.route <= 0L || 999999L < FbxCnf.route )
   {
      FbxCnf.route   = 0L;
      if( NO_ROUTE )
        E = -1;
   }
   if ( FbxCnf.run <= 0L || 999999L < FbxCnf.run )
   {
      FbxCnf.run     = 0L;
      if( NO_RUN )
         E = -1;
   }
   if ( FbxCnf.trip <= 0L || 999999L < FbxCnf.trip )
   {
      FbxCnf.trip    = 0L;
      if( NO_TRIP )
         E = -1;
   }
   if ( FbxCnf.dir <= 0 || 5 < FbxCnf.dir )
   {
      FbxCnf.dir     = 0;
      if( NO_DIR )
         E = -1;
   }
   if ( FbxCnf.stop <= 0L || 999999L < FbxCnf.stop )
   {
      FbxCnf.stop   = 0L;
      if( NO_STOP )
         E = -1;
   }
   if ( FbxCnf.city <= 0L || 999999L < FbxCnf.city )
   {
      FbxCnf.city   = 0L;
      if( NO_CITY )
         E = -1;
   }

   if ( scan_tc_exp() != -1 )
   { 
      Clist.PRIVATE.OpFlags |= C_CUT; 
      time( &t ); 

      if ( t >= FbxCnf.cuttm ) 
        E = -1;
      else
      {
      /* no restrictions on cut time for Santa Monica */
         if ( MyAgency != AGENCY_SANTA_MONICA )
         {
            if ( ( FbxCnf.cut[0] == 110 ) && ( FbxCnf.cut[0] == 110 ) )
               goto CutTimeDisabled;
            else if ( ( FbxCnf.cut[0] > 23 && FbxCnf.cut[0] < 100 ) || 
               FbxCnf.cut[1] > 59 )
              E = -1;
         }
      }
   }
   else
      Clist.PRIVATE.OpFlags &= ~C_CUT;


   CutTimeDisabled:

   if ( FbxCnf.busno <= 0L || 99999L < FbxCnf.busno )
      Ml.bus_n = Mlist.Config.BusNum = FbxCnf.busno = 0L;

   if ( FbxCnf.fbxno <= 0L || 99999L < FbxCnf.fbxno )
      Ml.fbx_n = Mlist.Config.FareBoxNum = FbxCnf.fbxno = 0L;


   if( FbxCnf.fareset >= 10 )          /* check if fareset valid         */
      FbxCnf.fareset = 0;              /* fareset fault detected, fix    */

   if ( E == 0 )
      Clr_Sflag( S_INVALIDCNF );
   else 
   {
      if( !NO_LOG || Tst_Sflag( S_BURNIN )) /* driver log in enabled ? */  
      {
         E = 0;         
         Clr_Sflag( S_INVALIDCNF );        /* no, clear invalid config flag */
      }
      else
      {
         if( !Tst_Sflag( S_INVALIDCNF ) && Driver_Rev )
            ++DoorDump;                   /* dump rev */                     
         Set_Sflag( S_INVALIDCNF );
      }
   }

   if ( VER_4 )
       Bill_Transport_Enabled = TRUE;
   else
   {
       for( i=0; i<=FE+TTP; i++ )
       {
           u.w = (word)(Dlist.FareTbl[FbxCnf.fareset][i+1]);
           if ( u.p.fare == BillTranEnable )
               break; 
       }   

       if ( i > FE+TTP )
           Bill_Transport_Enabled = TRUE;
   }

   return E;
}

void  ServiceTest( void )
{
   time_t       t;     
   struct tm    *tm; 

   static int   bf,d_dp,dp = 0;
   static long  cf;
   static char  display[9];
   int    i;

   Init_Tmr_Fn( SRVC_TEST_TMR, ServiceTest, SECONDS( 1 ) ); 

   if ( !J1708 )
      if( !PPS_ON || ( PPS_ON && (MyAgency != AGENCY_LEBANON) ) )
          dprintf( 1, 0, " " );         /* blank lower display */

   if ( !Tst_Sflag(S_SELFTEST) && tmr_expired(DISP_DELAY_TMR) )
   {
      if ( tmr_expired(IDLE_MISC_TMR)  )   /* in hibernation ? */ 
      {
         if( d_dp == TRUE )                /* yes, time to turn on '.' */
            d_dp = FALSE;                  /* no, get ready to turn off */
         else
         {
             d_dp = TRUE;                  /* get ready to turn on */
             if( ++dp > 7)                 /* location of decmial point */ 
               dp = 0; 
         }

         memset( display, ' ', sizeof(display)-1); /* blank display */
         display[8] = '\0';         

         if( d_dp == TRUE )          
            display[dp] = '.';            /* turn on decimal point, if time */
         dprintf( 0, 0, "%s", display  );
      }      
      else
      {      
         if( tmr_expired( DATA_ENTRY_TMR ) && !MachineOK() )
         {
            Mode = OUT_OF_SERVICE;              /* closed, display why */   
            dprintf( 2, 0, BLANK_DISPLAY );     /* blank 4 digit display */

            if ( Tst_Sflag( S_CASHBOX ) ) 
			   {
				   if ( Tst_Aflag(A_SPANISH ) )
					{
                  dprintf( 0, 0, "CAJA !  " );
                  if ( !Tst_Sflag(S_TRIMOFFLINE))
                     TrimPrintf( 0, 2, "CAJA !  " );
               }
				   else
					{
                  dprintf( 0, 0, "CASHBOX!" );
                  if ( !Tst_Sflag(S_TRIMOFFLINE))
                     TrimPrintf( 0, 2, "CASHBOX!" );
               }
			   }
            else if ( Tst_Sflag( S_DOOR ) ) 
			   {
				   if ( Tst_Aflag(A_SPANISH ) )
					{
                  dprintf( 0, 0, "ABIERTO " );
                  if ( !Tst_Sflag(S_TRIMOFFLINE))
                     TrimPrintf( 0, 2, "ABIERTO " );
               }
				   else
					{
                  dprintf( 0, 0, "UNLOCKED" );
                  if ( !Tst_Sflag(S_TRIMOFFLINE))
                     TrimPrintf( 0, 2, "UNLOCKED" );
               }
			   }
            else 
            {
               if ( NO_DRIVER )
               {
                  if ( MyAgency == AGENCY_SYRACUSE )
                    dprintf( 0, 0, "NO BADGE" );
                  else
				      {
					      if ( Tst_Aflag(A_SPANISH ) )
						      dprintf( 0, 0, "NO CHOFR" );
					      else
						      dprintf( 0, 0, "NO DRIVE" );
				      }
               }
               else if ( NO_ROUTE )
			      {
				      if ( Tst_Aflag(A_SPANISH ) )
					      dprintf( 0, 0, "NO RUTA " );
				      else
					      dprintf( 0, 0, "NO ROUTE" );
			      }
               else if ( NO_RUN )
               {
                   if( BLOCKENB )
                   {
                       if( MyAgency == AGENCY_WORCESTER )
                           dprintf( 0, 0, "NO TOWN " );
                       else
                           dprintf( 0, 0, "NO BLOCK" );
                   }
                   else        
                   {
                       if ( Tst_Aflag(A_SPANISH ) )
                           dprintf( 0, 0, "NO TURNO" );
                       else
                           dprintf( 0, 0, "NO RUN  " );
                   }
               }
               else if ( NO_TRIP )
			      {
				      if ( Tst_Aflag(A_SPANISH ) )
					      dprintf( 0, 0, "NO TREN " );
				      else
                  {
                    if( OF3_TRIP_BLOCK )
                       dprintf( 0, 0, "NO BLOCK" );
                    else
					        dprintf( 0, 0, "NO TRIP " );
                  }
	            }
               else if ( NO_DIR )
                   dprintf( 0, 0, "NO DIR  " );
               else if ( NO_CITY )
                   dprintf( 0, 0, "NO LOG  " );
               else if ( NO_STOP )
                   dprintf( 0, 0, "NO STOP " );
               else if ( ( FbxCnf.cut[0] == 110) && (FbxCnf.cut[1] == 110) )
               {

               }
               else if ( FbxCnf.cut[1] > 59 ||
                 ( FbxCnf.cut[0] > 23 && FbxCnf.cut[0] < 100 ) && NO_CUT )
                  dprintf( 0, 0, "NO CUT " );
               else if ( ( t >= FbxCnf.cuttm ) && NO_CUT ) 
                  dprintf( 0, 0, "BAD CUT " );
               else
                  dprintf( 0, 0, "INVLDCNF" );
            }
         }
         else
         {
            Mode = NORMAL;                          /* normal operation */
            dprintf( 2, 0, "%04d", Driver_Rev );    /* lite up numeric display  */      
            if ( Clist2.BillFull > 0 )
               bf = Clist2.BillFull; 
            else
               bf = 700;
            if ( Clist2.CoinFull > 0 )
               cf = Clist2.CoinFull*100; 
            else
               cf = 80000;

            if ( CashboxCoin > cf  )
            {
               if ( CoinFullFlag != COINS_FULL )
                  GPSSendStatus(COINS_FULL_EMR);
               CoinFullFlag = COINS_FULL;
            }
            else if ( CashboxCoin > ((cf * 75 )/100) )
            {
               if ( CoinFullFlag != COINS_75 )
                  GPSSendStatus(COINS_75);
               CoinFullFlag = COINS_75;
            }
            else
               CoinFullFlag = 0;

            if ( CashboxBill > bf )
            {
               if ( BillFullFlag != BILLS_FULL )
                  GPSSendStatus(BILLS_FULL_EMR);
               BillFullFlag = BILLS_FULL;
            }
            else if ( CashboxBill > ((bf * 75)/100) ) 
            {
               if ( BillFullFlag != BILLS_75 )
                  GPSSendStatus(BILLS_75);
               BillFullFlag = BILLS_75;
            }
            else
               BillFullFlag = 0;

            if ( Tst_Sflag( S_BYPASS ) ) 
               dprintf( 0, 0, "-BYPASS-" );
            else if ( TRIM_ENABLED && Tst_Sflag(S_TRIMOFFLINE))
              dprintf( 0, 0, "TRIM OFF" );
            else if ( !Tst_Sflag(S_TRIMOFFLINE) &&
               Tst_Sflag( S_TRIM_JAM ) && !MANUAL_TRIM ) 
            { 
              if ( Tst_Sflag( S_TRIMBYPASS ))
                dprintf( 0, 0, "TRM BYPS" );
              else
                dprintf( 0, 0, "MISFEED " );
            }
            else if ( !Tst_Sflag(S_TRIMOFFLINE) && Tst_Sflag( S_TRIMBYPASS ) )
              dprintf( 0, 0, "TRM BYPS" );
            else if ( (CashboxBill > bf  ) || ( CashboxCoin > cf ) )
            {
               if( !Tst_Sflag( S_FULLCASHBOX ) )     
               {
                   Set_Sflag( S_FULLCASHBOX );         /* 1st time detected */
                   NewEv( Ev_CB_FULL, 0 );             /* store event       */
               }
   			   if ( Tst_Aflag(A_SPANISH ) )
	   			   dprintf( 0, 0, "CODIGO 3" );
		   	   else
			   	   dprintf( 0, 0, " CODE 3 " );            /* display full message */ 
            }
            else if ( (i = DBfilling( Clist2.MemNearFull )) != 0 )
               dprintf( 0, 0, "MEM %d%%", i /*Clist2.MemNearFull */  );
            else if ( J1708 && (DriverLogonStatus != 0) )
            {
			      dprintf( 0, 0, "AMDT ALM" );        /* display full message */ 
            }  
            else                
            {  
               if( DISP_TIME ) 
               {
                  time( &t ); 
                  tm = gmtime( &t );  
               
                  display[0] = '\0';
                  strftime( display, sizeof(display)-1, "%I:%M", tm );
                  if( MyAgency == AGENCY_LITTLEROCK )
                  {
                     display[0] = '\0';
                     strftime( display, sizeof(display)-1, "%I:%M ", tm );
                     display[6] = ( tm->tm_hour < 12 ) ? 'A': 'P';
                     display[7] = 'M';
                     display[8] = '\0';
                     dprintf( 0, 0, display );
                  }   
                  else
                     dprintf( 0, 0, "%02d:%02d:%02d", tm->tm_hour, tm->tm_min, tm->tm_sec ); 
               }
               else
                  dprintf( 0, 0, " ");
            }
         } 
      }      
   }
   if( Tst_Sflag( S_SELFTEST) && Tst_Sflag( S_BURNIN ) )
   {
      if ( tmr_expired(SELF_TEST_TMR) || tmr_expired(BURN_IN_TMR ) )
      {
         InitCycleTest();        /* fault detected in self test, restart */     
         ++BurnReset;            
      }
   } 
}

void PPSPoll( void )
{
   PPSMsg( ID_ALL, MT_ONLINE, NULL, 0 );
   Init_Tmr_Fn( PPSTMR, PPSPoll, SECONDS( 5 ) );
}

static
int   CheckRamCard()
{
   register ulong   *p = (ulong*)0x400000L;     /* pointer to PCMCIA */

   for( p=(ulong*)0x400000L; p<(ulong*)0x410000L; p++ )
      *p = 0xfeedf00d;

   for( p=(ulong*)0x400000L; p<(ulong*)0x410000L; p++ )
   {
      if ( *p != 0xfeedf00d )
         break;
   }

   return( p < (ulong*)0x410000L );
}
