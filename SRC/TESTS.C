/*----------------------------------------------------------------------------\
|  Src File:   testsv09.c                                                     |
|  Authored:   11/09/93, cj, rz                                               |
|  Function:   Card test functions.                                           |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   03/16/95, tgh  -  changed header and filename.                 |
|      1.00:   04/06/95, tgh  -  added Hold().                                |
|      1.01:   05/15/97, sjb  -  correct processing rolling date pass         |
|      1.03:   04/04/98, sjb  -  change localtime to gmtime                   |
|      1.04:   05/10/98, sjb  -  add check for xfer auto issue                |
|                    NOTE: fare structure editor must be used to setup        |
|                          auto issue xfer or issue via xfer key (*11)        |
|      1.05:   07/27/98, sjb  -  bad list modified for 10 byte serial no.     |
|                                for use of credit cards & cards with credit  |
|                                card format                                  |
|      1.07:   01/31/01, aat  -  correct checks for holiday, saturday and     |
|                                sunday.                                      |
|      1.09:   07/01/02, sjb  -  add hooks to check for rolling start date    |
|                                used on smart cards                          |
|                             -  check for media restirctions for friendly    |
|                                agencies                                     |
|      1.10:   01/21/04, sjb  -  check ttp limits in friendly agency function |
|           Copyright (c) 1994 - 2002 GFI All Rights Reserved                 |
\----------------------------------------------------------------------------*/
/* System headers
*/
#include <stdio.h>
#include <string.h>
#include <time.h>

/* Project headers
*/
#include "gen.h"
#include "cnf.h"
#include "mag.h"
#include "rtc.h"
#include "dll.h"
#include "sclk.h"
#include "port.h"
#include "tests.h"
#include "transact.h"
#include "magcard.h"
#include "misc.h"
#include "scard.h"


/* Define(s)
*/
#define  CardType( p )        (p->Group<<5 | p->Desig&0x1f)
   /* media list */
#define  AttrFlag( ndx, f )   (Dlist.MediaList[(ndx)].Attr & (f))
#define  SUNDAY         0
#define  SATURDAY       6
/*
#define  GOODPEAK       0x01                    /* attribute flags */
/*
#define  GOODOFFPEAK    0x02
#define  GOODWKD        0x04
#define  GOODSAT        0x08
#define  GOODSUN        0x10
#define  GOODHOL        0x20
#define  GOODFRIENDLY   0x40
#define  DISABLED       0x80
*/
   /* transfer control table */
#define  TCAttrFlag( ndx, f ) (Dlist.TransferControl[(ndx)].Attr & (f))
#define  ODSR           0x01
#define  SDSR           0x02
#define  ODDR           0x04
#define  SDDR           0x08
#define  CAP0           0x10
#define  HOLD           0x20
#define  UPGRADE        0x40
#define  NO_AUTOXFER    0x80

/* Prototype(s)
*/


/*
** check incomming card for valid start, stop, and expiration dates
*/
ulong is_date_ok( CARD *p )
{
  ulong  rc = 0;
  time_t now = time( (time_t*)NULL );

  if( p->FirstUse == 1)  /* rolling date pass ? */
    rc |= NVY;           /* rolling date pass not good till used in trim */
  if( p->StartDate > now )
    rc |= NVY;
  if( now > p->AbsExp )
    rc |= EXP;
  if( now > p->Expiration )
    rc |= EXP;

  return rc;
}


/*
** check incomming card for valid start, stop, and expiration dates
*/
ulong is_sc_date_ok( CARD *p )
{
  ulong  rc = 0;
  time_t now = time( (time_t*)NULL );

  if( p->StartDate == ROLLING_START )
  {
    p->StartDate = now / DaySec;
    p->FirstUse = 1;
  }

  if( p->StartDate > now )
    rc |= NVY;
  if( now > p->AbsExp )
    rc |= EXP;
  if( now > p->Expiration )
    rc |= EXP;

  return rc;
}


/*
** check for bad list violation
*/
int isbadlisted( CARD *c, B_SERIAL *s )
{
  if( c->BadFlag )
    return 1;
  if( scan_bad_list( s ) )
    return 1;
  if( scan_bad_ranges( s ) )
    return 1;
  return 0;
}

/* Checks are performed in the following order :
      holiday
      sunday
      saturday
      weekday
         peak
         offpeak
*/
ulong check_card_restrictions( int index )
{
   ulong       rc    = 0L;
   time_t      now   = time( (time_t*)NULL );
   struct tm  *t     = gmtime( &now );
   int         peak, holiday;

   if ( AttrFlag( index, DISABLED ) )
   {
      rc |= PED;
      goto function_end;
   }

   holiday = IsHoliday( t );

   if ( holiday )
   {
       if( !AttrFlag( index, GOODHOL ) )
           rc |= HOL;

       goto function_end;
   }
   
   if ( t->tm_wday == SUNDAY )
   {
       if ( !AttrFlag( index, GOODSUN ) )
           rc |= SUN;

       goto function_end;
   }
   
   if ( t->tm_wday == SATURDAY )
   {
       if ( !AttrFlag( index, GOODSAT ) )
           rc |= SAT;

       goto function_end;
   }

   if ( !AttrFlag( index, GOODWKD ) )           /*assume weekday at this point*/
   {
      rc |= WKD;
      goto function_end;
   }

   peak = IsPeak( t );

   if (  peak && !AttrFlag( index, GOODPEAK ) )
   {
      rc |= PK;
      goto function_end;
   }

   if ( !peak && !AttrFlag( index, GOODOFFPEAK ) )
   {
      rc |= OPK;
      goto function_end;
   }

   function_end:
   return rc;
}

/*
** check incomming card if can be used with friendly agencies
*/
int  is_friendly_ok( CARD *p )
{
   if ( p->TTP_used >= 0 && p->TTP_used < TTP )
      return( AttrFlag( p->TTP_used, GOODFRIENDLY ) );
   else
      return 0;
}

/*
** check a transfer for restriction violations
*/
ulong check_xfer_restrictions( int index, CARD *card )
{
  ulong  rc = 0L;
  byte       dir;

  dir = FbxCnf.dir;
  if ( dir )
     --dir;       
  
  /* check opposite direction same route */
  if( (Dlist.TransferControl[index].Attr & ODSR) == 0 )
  {
    if( (dir+card->IUDir == 3) && (FbxCnf.route == card->IURoute) )
    {
      rc |= TODSR;
    }
  }

  /* check same direction same route */
  if( (Dlist.TransferControl[index].Attr & SDSR) == 0 )
  {
    if( (dir == card->IUDir) && (FbxCnf.route == card->IURoute) )
    {
      rc |= TSDSR;
    }
  }

  /* check opposite direction different route */
  if( (Dlist.TransferControl[index].Attr & ODDR) == 0 )
  {
    if( (dir+card->IUDir == 3) && (FbxCnf.route != card->IURoute) )
    {
      rc |= TODDR;
    }
  }

  /* check same direction different route */
  if( (Dlist.TransferControl[index].Attr & SDDR) == 0 )
  {
    if( (dir == card->IUDir) && (FbxCnf.route != card->IURoute) )
    {
      rc |= TSDDR;
    }
  }

  return rc;
}

int   Capture( int index )
{
   return( TCAttrFlag( index, CAP0 ) );
}

int   Hold( int index )
{
   return( TCAttrFlag( index, HOLD ) );
}

int   Upgrade( int index )
{
   return( TCAttrFlag( index, UPGRADE ) );
}

int   Autoxfer( int index )
{
   if ( MyAgency == AGENCY_LITTLEROCK || MyAgency == AGENCY_MTC )
      return( TCAttrFlag( index, NO_AUTOXFER ) );
   else
      return( TRUE );
}

static
void  copy_v1_to_v2( int media )
{
	if( media )
	{
	   memcpy(((void*)SCVDB0_2),((void*)SCVDB0_1),sizeof(SCVDB0));
	   *((char*)(SCVDB0_2)-1)               = StartSent;
	   *((char*)(SCVDB0_2)+sizeof(SCVDB0))  = EndSent;
	}
	else
	{
		memcpy(((void*)VDB0_2),((void*)VDB0_1),sizeof(VDB0));
		*((char*)(VDB0_2)-1)             = StartSent;
		*((char*)(VDB0_2)+sizeof(VDB0))  = EndSent;
	}
}

static
void  copy_v2_to_v1( int media )
{
	if( media )
	{
	   memcpy(((void*)SCVDB0_1),((void*)SCVDB0_2),sizeof(SCVDB0));
	   *((char*)(SCVDB0_1)-1)               = StartSent;
	   *((char*)(SCVDB0_1)+sizeof(SCVDB0))  = EndSent;
	}
	else
	{
		memcpy(((void*)VDB0_1),((void*)VDB0_2),sizeof(VDB0));
		*((char*)(VDB0_1)-1)             = StartSent;
		*((char*)(VDB0_1)+sizeof(VDB0))  = EndSent;
	}
}

/*
** Checks and corrects, if possible, the data from the
** variable track after a read.  Return codes show errors
*/
word  validate_variable_info( int media )
{
   word  rc = 0;

   if( media )
   {
	   /* SmartCard */
	   if ( crc16((char*)SCVDB0_1, sizeof(SCVDB0)) ) rc |= REV1;
	   if ( crc16((char*)SCVDB0_2, sizeof(SCVDB0)) ) rc |= REV2;
   }
   else
   {
	   /* Magnetics */
	   if ( crc16((char*)VDB0_1, sizeof(VDB0)) ) rc |= REV1;
	   if ( crc16((char*)VDB0_2, sizeof(VDB0)) ) rc |= REV2;
   }

   switch( rc )
   {
      case 0:
         break;
      case REV1:
         copy_v2_to_v1(media);
         break;
      case REV2:
         copy_v1_to_v2(media);
         break;
      default:
         rc |= URE;
         break;
   }

   return rc;

}


