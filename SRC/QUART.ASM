*-----------------------------------------------------------------------------+
*  Src File:   quart.asm                                                      |
*  Version :   3.01                                                           |
*  Authored:   01/11/96, tgh                                                  |
*  Function:   Interrupt handler for quart.                                   |
*  Comments:   fixme, clean this up !                                         |
*                                                                             |
*  Revision:                                                                  |
*      1.00:   08/04/96, tgh  -  Initial release.                             |
*      2.00:   12/02/98, tgh  -  Move port D (probe) up in the irq processing.|
*      3.00:   03/10/05, sjb  -  run quart at full speed                      |
*      3.01:   03/17/05, sjb  -  enable FIFO on transmitter                   |
*                                                                             |
*           Copyright (c) 1993-1996  GFI/USPS All Rights Reserved             |
*-----------------------------------------------------------------------------+

        include gen.inc
        include regs.inc
        include quart.inc

*---------------------------------------------------------------------------+
*                       Local Macros                                        |
*---------------------------------------------------------------------------+

*       Read a quart register leaving result in reg. d0
*       usage: getreg n,reg
*       n     - quart number (0,1,2)
*       reg   - register offset
        getreg:         macro
                        move.b  quart\1base+\2,d0
                        endm

*       Write the contents on reg. d0 to a quart register.
*       usage: putreg n,reg,value
*       n     - quart number (0,1,2)
*       reg   - register offset
*       value - byte to send
        putreg:         macro
                ifnc '\3',''
                        move.b  \3,d0
                endc
                        move.b  d0,quart\1base+\2
                        endm

*-----------------------------------------------------------------------------+
*                       Data Segment (local data)                             |
*-----------------------------------------------------------------------------+
               section  udata,,"data"           uninitialized data

        quart0_imr1_value:      ds.b    1       imr value for quart 0 a/b
        quart0_imr2_value:      ds.b    1       imr value for quart 0 c/d

        xdef    quart0_imr1_value,quart0_imr2_value

*-----------------------------------------------------------------------------+
*                       External Regerences                                   |
*-----------------------------------------------------------------------------+
        xref    process_dbg_comm,process_485_comm
        xref    process_prb_comm,ProcessRS232Comm

*-----------------------------------------------------------------------------+
*                       Code Segment                                          |
*-----------------------------------------------------------------------------+
                section S_Quart0Irq,,"code"

*-----------------------------------------------------------------------------+
*  Function:   Quart0Irq                                                      |
*  Purpose :   Process interrupts for the first two quarts.                   |
*  Synopsis:                                                                  |
*  Input   :   None.                                                          |
*  Output  :   None.                                                          |
*  Register:   All preserved.                                                 |
*  Comments:                                                                  |
*-----------------------------------------------------------------------------+
        _fnct   Quart0Irq

                movem.l d0-d7/a0-a6,-(a7)       save registers

                getreg  0,isr2                  check quart 0 D
                and.b   quart0_imr2_value,d0    check only the
                andi.b  #0f0h,d0                enabled interrupts
                beq.s   check_0a                not this one, get out
                jsr     process_prb_comm        process probe port

                putreg  0,imr2,quart0_imr2_value be sure ...
                bra     irq_end                 fixme just to try

  check_0a:
                getreg  0,isr1                  check quart 0 A
                and.b   quart0_imr1_value,d0    check only the
                andi.b  #00fh,d0                enabled interrupts
                beq.s   check_0b                not this one, check quart 0 B
                jsr     ProcessRS232Comm

  check_0b:
                getreg  0,isr1                  check quart 0 B
                and.b   quart0_imr1_value,d0    check only the
                andi.b  #0f0h,d0                enabled interrupts
                beq.s   check_0c                not this one, check quart 0 c
                jsr     process_dbg_comm        process debug port

  check_0c:
                getreg  0,isr2                  check quart 0 C
                and.b   quart0_imr2_value,d0    check only the
                andi.b  #00fh,d0                enabled interrupts
                beq.s   check_end               not this one, get out
                jsr     process_485_comm        process rs485 port

  check_end:
                putreg  0,imr1,quart0_imr1_value be sure ...
                putreg  0,imr2,quart0_imr2_value be sure ...

  irq_end:

                movem.l (a7)+,d0-d7/a0-a6       restore registers

                rte



*-----------------------------------------------------------------------------+
*  Function:   InitializeQUART                                                |
*  Purpose :   Initialize data and states common to all quart ports.          |
*  Synopsis:                                                                  |
*  Input   :   None.                                                          |
*  Output  :   None.                                                          |
*  Register:   All preserved.                                                 |
*  Comments:                                                                  |
*-----------------------------------------------------------------------------+
        _fnct   _InitializeQUART

*                putreg  0,crc,#0d0h             ; Divide System Clock
                putreg  0,crc,#0c0h             ; System Clock at full speed

                putreg  0,imr1,#0               ; disable interrupts
                putreg  0,imr2,#0               ; disable interrupts
                putreg  0,cra,#00111010b        ; disable rx & tx port A
                putreg  0,cra,#00100000b        ; reset rx
                putreg  0,crb,#00111010b        ; disable rx & tx port B
                putreg  0,crb,#00100000b        ; reset rx
                putreg  0,crc,#00111010b        ; disable rx & tx port A
                putreg  0,crc,#00100000b        ; reset rx
                putreg  0,crd,#00111010b        ; disable rx & tx port B
                putreg  0,crd,#00100000b        ; reset rx

                putreg  0,cra,#11100000b        ; set special MR pointer
*                                                 shadow mode registers
                putreg  0,mra,#00000100b        ; enable TxD FIFO
                putreg  0,mrb,#00000100b        ; enable TxD FIFO
                putreg  0,mrc,#00000100b        ; enable TxD FIFO
                putreg  0,mrd,#00000100b        ; enable TxD FIFO
                putreg  0,cra,#11110000b        ; reset special MR pointer

                clr.b   quart0_imr1_value       ; initialize imr value(s)
                clr.b   quart0_imr2_value       ; initialize imr value(s)


             rts
