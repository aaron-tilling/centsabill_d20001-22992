/*---------------------------------------------------------------------------\
|  Src File:  mainv301.c                                                     |
|  Version :  3.01                                                           |
|  Authored:  01/09/96, tgh                                                  |
|  Function:  Main module for generic farebox with new logic board.          |
|  Comments:                                                                 |
|                                                                            |
|  Revision:                                                                 |
|        2.93: 10/13/04, sjb - add code to adjust Periodic Event timer based |
|                              on time farebox was off.NOTE: not implemented |
|                              at this time because menus not in to select   |
|                              options at fb                                 |
|        3.01: 08/05/04, sjb - add hooks to modify daylight option flag      |
|                              modify quarts to run system clock direct,     |
|                              previous versions used divided clock          |
|                              add hooks select 230.4k baud for probe port   |
|                                                                            |
|          Copyright (c) 1993-2003  GFI/USPS All Rights Reserved             |
\---------------------------------------------------------------------------*/
/* System headers
*/
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <rcopy.h>
#include <time.h>

/* Project headers
*/
#include "gen.h"
#include "timers.h"
#include "rtc.h"
#include "sclk.h"
#include "led.h"
#include "bill.h"
#include "door.h"
#include "lamp.h"
#include "coin.h"
#include "util.h"
#include "pps.h"
#include "dbase.h"
#include "transact.h"
#include "fbd.h"
#include "regs.h"
#include "mag.h"
#include "magcard.h"
#include "pbq.h"
#include "diag.h"
#include "status.h"
#include "display.h"
#include "cnf.h"
#include "ability.h"
#include "fare.h"
#include "menu.h"
#include "probe.h"
#include "trim.h"
#include "j1708.h"
#include "rs485.h"
#include "misc.h"

extern   ClearDebugStatus();

/* Defines
*/ 
/*  */
#define  PART_NUM  22992           /* program part no. -               */
                                   /* if generic - 22999 with lb 20001 */
                                   /* 229998 with lb 20750             */
                                   /* if customer specific, set in     */
                                   /* test_vxx based on AgencyDefault  */

/* Defined by library for initializing static data
*/
#pragma  separate    rompOutSeg
extern   int         rompOutSeg;

extern   int         BadClock;
extern   int         CRD_Debug;


long     LogicBoard;                           /* logic board being used    */
int      MyOldAgency;                          /* Host Old agency ID        */
int      MyAgency;                             /* Host agency ID            */

/*----------------------------------------------\
|  Function:   main()                           |
|  Purpose :   Main loop (background).          |
|  Synopsis:   void  main( void );              |
|  Input   :   None.                            |
|  Output  :   None.                            |
|  Comments:   Called from _pmain.              |
\----------------------------------------------*/
void  main()
{ 
   time_t      t,t_off,off_time;
   struct tm   *now,old_t;
   int         i;

   time( &t_off );                      /* fetch time stopped */

   if ( Mlist.Config.FareBoxVer != Version )
   {
     CashboxCoin  = CashboxRevenue = 0L;/* clear cashbox coin rev     */
     CashboxBill  = 0;                  /* clear cashbox bill count   */
     Driver_Rev   = 0;                  /* clear drivers revenue      */
     Magval       = 0;                  /* clear mag rev              */   
     BadClock     = 0;                  /* clear clock flags          */
     dls_window   = 0;
     BadBatteryDetect = 0; 
     Clr_Sflag( S_FULLCASHBOX );        /* 1st time detected */
     time( &t_off );                    /* fix time stopped           */
     memset( (char*)&J1708Diag, 0, sizeof(J1708DIAG) );
     InitializeSensors( 1 );    
   }

   Ml.fbx_v  = Mlist.Config.FareBoxVer = Version; /* fetch current version  */
   if ( LogicBoard == 20001 )
      Ml.fbx_v += 2000;
   else
      Ml.fbx_v += 1000;

   if ( Prologue() )                    /* memory error detected    */
   {
     time( &t_off );                    /* fix time stopped   */
     init_pbq( TRUE );                  /* flush que          */
     BadClock = 0;                      /* int bad clock flag */
     BadBatteryDetect = 0; 
     dls_window = 0;
   }  
   else
   {
      time( &t );                              /* fetch time started */
 
      off_time = labs(t - t_off); 
      if( off_time > MINN_OFF_TM )      /* off over 2 minutes */
      {
         NewEv( Ev_COLD_START, 0 );           /* yes, store event   */
         Ev.t = t_off;
         NewEv( Ev_POWER_RESTORED, 0 );
      } 

      if( off_time > AUTO_LOGOFF_TM )        /* off for max hours */
         AutoDriverLogOff();                       /* then log off driver */

      if( off_time >  300 )                  /* if off for 5 min */
         init_pbq( TRUE );                         /* flush que         */

      if ( FB_TIMED_EV )
      {
         i = off_time % 300;                   /* add varible event time timer */
         Ev_Timer_Counter + i;
      }
      else
         Ev_Timer_Counter = 0;

   /* check if went past dst or std while off */

      if ( DLS_TIME )
      { 
         now = localtime( &t_off );                   /* check old time if dls */ 
         old_t  = *now;
         now = localtime( &t );                       /* check new time if dls */ 

         if ( old_t.tm_isdst != now->tm_isdst )       /* compare times */
         {
            if( now->tm_isdst )                       /* went from std to dst */
            {
               SetRtc( now );                         /* advance 1 hour */
            }
            else                                     /* went from dst to std */
            {
               if( now->tm_hour == 1 && now->tm_wday == 0 && !dls_window ) 
                 dls_window = TRUE;  
               else
               {
                  if ( !dls_window )
                  {
                     t -= HrsSec;
                     now = localtime( &t );       /* check new time if dls */ 
                     SetRtc( now );               /* back 1 hour */
                  }
               }
            } 
         }
      }
   }      

   _SPL( 0 );                                   /* enable interrupts */

   if( kbhit() )                                /* flush debug port */
      getchar();

   switch( rsr.w & 0x00ff )                     /* reset source */
   {
      case 0x80:                                /* external reset source */
      case 0x40:                                /* power fail */
         Mlist.System.ColdStart++;
         Ml.cold_c++;
         break;
      case 0x20:                                /* software watchdog */
         Mlist.System.WarmStart++;
         Ml.warm_c++;
         break;
      default:
         break;
   }

   if ( (Ml.warm_c + Ml.cold_c) < 20L )
   {
      if ( labs(t - t_off) > 2 )
         T_event( FBXPWRL, (char*)&t_off, 4 );
      T_power_up();
   }

   for( i = FE+ATTR; i < FE+TTP+ATTR; ++i)
   {
      if( (Dlist.FareTbl[FbxCnf.fareset][i] & 0x0fff) == CPU_RESET )
         CountFare( i );
   }
   Set_Sflag( S_TRIMOFFLINE );             /* set trim offline */ 
   CheckEntry();                       /* validate entries in current event */

   for(;;)
   {
      Event();
      Coin_Throat();
      Bill_Throat();
      CardProcessing();
      Dump_Button_Test();
      Door_Open_Test();
      CheckSound();
      Check_Key();
      Test_Jumper_Check();
      Lamp();
      CalculateCoin();
      ProcessBill();
      FareProcess();

      if ( Tst_Aflag( A_POWER ))
      {
        if ( ! Tst_Aflag( A_P_DETECT ))
        {
           /* save possible low power transaction */ 
        /*   printf("low power fault\n"); */
        }
        Set_Aflag( A_P_DETECT );
      }
      else
      {
        Clr_Aflag( A_P_DETECT );
      }
   }   
}

/*----------------------------------------------\
|  Function:   Prologue()                       |
|  Purpose :   Power-up initialization.         |
|  Synopsis:   int   Prologue( void );          |
|  Input   :   None.                            |
|  Output  :   int error_code;                  |
|  Comments:                                    |
\----------------------------------------------*/
int   Prologue()
{
   int   E=0;

   rcopy( &rompOutSeg );

   Initialize_Gpt();
   Initialize_Qsm();
   Initialize_PortF();
   CheckCnf();
   if ( tmr_expired( BILL_ESCROW_TMR ) )
      Initialize_Timers();
   else
   {
     Initialize_Timers(); 
     /* reload escrow timer */
     if ( Clist2.BillTimeOut == 0 )
         Init_Tmr_Fn( BILL_ESCROW_TMR, Bill_Advance, SECONDS( 15 ) ); /* init timeout function */
     else
         Init_Tmr_Fn( BILL_ESCROW_TMR, Bill_Advance, SECONDS( Clist2.BillTimeOut ) ); /* init timeout function */
   }
   if( Bill_Advance_Cnt != 0 )  /* bill advance ? */
   {
     Init_Bill_Irq3();              /* yes, initialize irq3 interrupt */      
     Init_Tmr_Fn( BILL_THROAT_TMR, 0, SECONDS( 10 ) );
   }

   if ( VER_4 )
      Clist2.BaudRate = B9600;          /* default to 9600 */

   DCU_TimeOutCnt = 0;               /* init DCU/TRIM error count */ 
   TRM_Debug = 0;
   TRANS_Debug = 0;
   J1708_Debug = 0;
   CRD_Debug = 0;
   switch( Trim_DCU_Select )         /* check and fix selected port */
   {
      case 0:                        /* trim selected */
      case B_19200:                  /* dcu selected  */
        break;
      default:
        Trim_DCU_Select = 0;          /* default to trim */
        break;
   }

   switch(Clist2.BaudRate)
   {
      case B19200:
        BaudRate = B_19200;            /* save new baud rate */
        break;
      case B38400:
        BaudRate = B_38400;            /* save new baud rate */
        break;
      case B57600:
        BaudRate = B_57600;            /* save new baud rate */
        break;
      case B115200:
        BaudRate = B_115200;            /* save new baud rate */
        break;
      case B230400:
        BaudRate = B_230400;            /* save new baud rate */
        break;
      case B9600:
      default:
        BaudRate = B_9600;            /* save new baud rate */
        Clist2.BaudRate = B9600;
        break;
   }

   InitializeQUART();
   Initialize_Dbg_Comm();
   InitializeRS232Comm();
   initialize_prb_comm(0);
   InitializeRS485Com(Trim_DCU_Select);    /* init 485 port   */
   InitializeSciComm( J1708 );

   InitializeSonyProtocol();
   InitializeHDLCProtocol();
   InitializePPSProtocol(); 
   Initialize485PPSProtocol();

   if ( Trim_DCU_Select == 0 )             /* TRiM selected ? */
      Initialize485Protocol();             /* yes             */

   crc_table(); 
   crc_table32(); 

   init_pbq( 0 );
   Initialize_Rtc();
   Initialize_Sclk();
   Init_Display();
   Init_Card_Irq1(); 
   Init_Key_Irq2();
   Init_CBID_Irq4( 1 ); 
   InitCycleTest();
   if ( InitializeStatus() )
      E = TRUE;
   if ( InitializeCnf() )
      E = TRUE;

   InitializeSensors( E );    

   Ml.bus_n = Mlist.Config.BusNum = FbxCnf.busno;       /* make sure bus no. updated        */ 
   Ml.fbx_n = Mlist.Config.FareBoxNum = FbxCnf.fbxno;   /* make sure fb no. updated         */ 

   memset( (char*)&Trim_Diag, 0, sizeof(Trim_Diag) );

   Clist.PRIVATE.PartNo = PART_NUM;
   switch( PART_NUM )
   {
      case 22999:
         if ( LogicBoard == 20750 )
            Clist.PRIVATE.PartNo -= 1;
         break;
      default:
         break;
   }

   if ( Clist.PUBLIC.AgencyCode > AGENCY_GFI || Clist.PUBLIC.AgencyCode == 0 )
   {
      Clist.PUBLIC.AgencyCode = MyAgency = AgencyDefault;
      MyOldAgency = OLDAGENCY_DISABLED;
   }
   else
   {
      MyOldAgency = Clist2.OldAgencyCode;
      MyAgency    = Clist.PUBLIC.AgencyCode;    /* ver 243 */
   }

   Mlist.Config.TrimVer = Ml.trim_v = 0;  /* init till received from trim   */  
   Mlist.Config.TrimNum = Ml.trim_n = 0L; /* init till received from trim   */  
   CurrentFare          = 0;              /* init current fare to full fare */
   Pending_xfer         = FALSE;          /* clear pendig xfer flag         */

   ConfigureEntry();

   ClearDebugStatus();
   return E;
}

