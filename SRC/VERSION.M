OBJS   = \
 main.ol commp.ol dbgc.ol dbgp.ol diag.ol \
 port.ol rtc.ol sclk.ol disp.ol cthrt.ol \
 bill.ol door.ol sound.ol card.ol keyp.ol \
 lamp.ol coin.ol sscp.ol tlog.ol cbid.ol \
 485p.ol prbc.ol prbp.ol scic.ol scip.ol \
 data.ol test.ol menu.ol msgs.ol dbase.ol \
 util.ol dispm.ol fare.ol dll.ol misc.ol \
 tests.ol mgctl.ol stat.ol trans.ol regs.ol \
 tmrs.ol pbqp.ol 

OBJ1   = \
 rboot.ol hdlc.ol mgdsp.ol scpr.ol scctl.ol \
 dcu.ol sctst.ol dmenu.ol version.ol
  
OBJ2   = \
 pmain.ol vect.ol fpga.ol quart.ol 232c.ol \
 485c.ol boot.ol flash.ol  

OBJ3   = \
 pmain2.ol vect2.ol fpga2.ol quart2.ol 232c2.ol \
 485c2.ol  

XPATH  = \itools

AS     = $(XPATH)\x\asm68332
CC     = $(XPATH)\x\c68332
LINK   = $(XPATH)\x\llink
FORM   = $(XPATH)\x\form
MAP    = $(XPATH)\x\gsmap

INC    = ..\inc

CFLAGS = -S $(XPATH)\rtlibs\lib332\inc -S $(INC) -ia -cs $(dbgflg)
AFLAGS = -S $(INC) -ex -l
LFLAGS = -i version.lnk -L $(XPATH)\rtlibs\lib332\lib\lib332 -rs idata -b _rompOutSeg -o p.ab -c loc_new.cmd
LFLAG2 = -i ver750.lnk -L $(XPATH)\rtlibs\lib332\lib\lib332 -rs idata -b _rompOutSeg -o p750.ab -c loc2_new.cmd
FFLAGS = -f xm -w 80000
MFLAGS = -n -o

.asm.ol     :  
               $(AS) $*.asm $(AFLAGS)

boot.ol     :  boot.asm $(INC)\pmain.inc $(INC)\regs.inc $(INC)\gen.inc

pmain.ol    :  pmain.asm $(INC)\pmain.inc $(INC)\regs.inc $(INC)\gen.inc

pmain2.ol   :  pmain2.asm $(INC)\pmain.inc $(INC)\regs.inc $(INC)\gen.inc

fpga.ol     :  fpga.asm

fpga2.ol    :  fpga2.asm

vect.ol     :  vect.asm $(INC)\gen.inc

vect2.ol    :  vect2.asm $(INC)\gen.inc

quart.ol    :  quart.asm $(INC)\gen.inc $(INC)\regs.inc $(INC)\quart.inc

quart2.ol   :  quart2.asm $(INC)\gen.inc $(INC)\regs.inc $(INC)\quart.inc

dbgc.ol     :  dbgc.asm $(INC)\gen.inc $(INC)\regs.inc $(INC)\comm.inc $(INC)\quart.inc

port.ol     :  port.asm $(INC)\gen.inc
          
232c.ol     :  232c.asm $(INC)\gen.inc $(INC)\regs.inc $(INC)\comm.inc $(INC)\quart.inc

232c2.ol    :  232c2.asm $(INC)\gen.inc $(INC)\regs.inc $(INC)\comm.inc $(INC)\quart.inc

485c.ol     :  485c.asm $(INC)\gen.inc $(INC)\regs.inc $(INC)\comm.inc $(INC)\quart.inc

485c2.ol    :  485c2.asm $(INC)\gen.inc $(INC)\regs.inc $(INC)\comm.inc $(INC)\quart.inc

prbc.ol     :  prbc.asm $(INC)\gen.inc $(INC)\regs.inc $(INC)\comm.inc $(INC)\quart.inc

scic.ol     :  scic.asm $(INC)\gen.inc $(INC)\regs.inc $(INC)\comm.inc

misc.ol     :  misc.asm $(INC)\gen.inc $(INC)\regs.inc

flash.ol    :  flash.asm $(INC)\gen.inc $(INC)\regs.inc $(INC)\comm.inc $(INC)\quart.inc
          
rboot.ol    :  rboot.asm

.c.ol       :
               $(CC) $*.c $(CFLAGS)

main.ol     :  main.c $(INC)\led.h $(INC)\magcard.h $(INC)\mag.h $(INC)\menu.h\
               $(INC)\misc.h

regs.ol     :  regs.c $(INC)\regs.h

tmrs.ol     :  tmrs.c $(INC)\timers.h $(INC)\coin.h $(INC)\bill.h $(INC)\ability.h

commp.ol    :  commp.c $(INC)\comm.h

dbgp.ol     :  dbgp.c

rtc.ol      :  rtc.c

sclk.ol     :  sclk.c

disp.ol     :  disp.c

cthrt.ol    :  cthrt.c $(INC)\coin.h $(INC)\port.h

coin.ol     :  coin.c $(INC)\coin.h $(INC)\port.h $(INC)\util.h $(INC)\fbd.h

bill.ol     :  bill.c $(INC)\bill.h $(INC)\port.h $(INC)\fbd.h

door.ol     :  door.c $(INC)\door.h $(INC)\port.h

sound.ol    :  sound.c $(INC)\sound.h $(INC)\port.h

card.ol     :  card.c $(INC)\port.h
    
keyp.ol     :  keyp.c $(INC)\port.h $(INC)\menu.h

lamp.ol     :  lamp.c $(INC)\lamp.h $(INC)\bill.h 
 
sscp.ol     :  sscp.c

tlog.ol     :  tlog.c

485p.ol     :  485p.c

prbp.ol     :  prbp.c $(INC)\fbd.h

data.ol     :  data.c $(INC)\fbd.h

test.ol     :  test.c $(INC)\mag.h 

tests.ol    :  tests.c

menu.ol     :  menu.c $(INC)\menu.h $(INC)\msgs.h

msgs.ol     :  msgs.c $(INC)\msgs.h

dbase.ol    :  dbase.c $(INC)\transact.h $(INC)\gen.h

util.ol     :  util.c $(INC)\util.h $(INC)\port.h

dispm.ol    :  dispm.c $(INC)\menu.h

fare.ol     :  fare.c $(INC)\menu.h $(INC)\sound.h $(INC)\transact.h\
                $(INC)\fbd.h
 
dll.ol      :  dll.c $(INC)\menu.h

mgctl.ol    :  mgctl.c $(INC)\menu.h

pbqp.ol     :  pbqp.c $(INC)\pbq.h
 
diag.ol     :  diag.c $(INC)\menu.h $(INC)\bill.h $(INC)\coin.h $(INC)\rs485.h

cbid.ol     :  cbid.c $(INC)\timers.h $(INC)\transact.h $(INC)\ability.h 

scip.ol     :  scip.c $(INC)\j1708.h

stat.ol     :  stat.c $(INC)\status.h $(INC)\timers.h 

trans.ol    :  trans.c $(INC)\transact.h

hdlc.ol     :  hdlc.c

mgdsp.ol    :  mgdsp.c

scpr.ol     :  scpr.c

scctl.ol    :  scctl.c

dcu.ol      :  dcu.c

dmenu.ol    :  dmenu.c

sctst.ol    :  sctst.c

version.ol  :  version.c

p.ab        :  $(OBJS) $(OBJ1) $(OBJ2)
               $(LINK) $(LFLAGS)

p.s19       :  p.ab
               $(FORM) p.ab $(FFLAGS) -o p.s19

p.map       :  p.s19
               $(MAP)  p.ab $(MFLAGS)

p750.ab     :  $(OBJS) $(OBJ1) $(OBJ3)
               $(LINK) $(LFLAG2)

p750.s19    :  p750.ab
               $(FORM) p750.ab $(FFLAGS) -o p750.s19

p750.map    :  p750.s19
               $(MAP)  p750.ab $(MFLAGS)

