/*----------------------------------------------------------------------------\
|  Src File:   msgs_v06.c                                                     |
|  Authored:   10/11/93, cj, rz                                               |
|  Function:   Display messages.                                              |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   09/20/94, tgh  -  changed header and filename.                 |
|      1.03:   10/04/94, sjb  -  changed rte/dir errors to be more generic.   |
|      1.06:   11/07/03, sjb  -  changed Unlisted of tranfer in transfer      |
|                                control table to "T UNLIST"                  |
|                                                                             |
|           Copyright (c) 1993, 1994 GFI All Rights Reserved                  |
\----------------------------------------------------------------------------*/
/* System headers
*/
#include <stdio.h>

/* Project headers
*/
                                   
/*
** These are global strings that are used throughout the program
*/

const char *DirMsg[] = { "    ","NORTH", "WEST", "EAST", "SOUTH" };
const char  DirLet[] = { " NWES" };

/*
** Card processing error codes
*/
const char *PEC[] = { "PASSBACK", "BAD AGCY", "SECURITY", "BAD LIST",
                      "UNLISTED", "NOT USED", "INVALID" , "EXPIRED" ,
                      "PEAK"    , "OFF PEAK", "WEEKDAY" , "SATURDAY",
                      "SUNDAY"  , "HOLIDAY" , "NO RIDES", "NO VALUE",
                      "PASSBACK", "T AGENCY", "T UNLIST", "T EXPIR" ,
                      "RTE/DIR" , "RTE/DIR" , "RTE/DIR" , "RTE/DIR" ,
                      "NO RIDES", "NOT USED", "T UNLIST", "BAD LIST",
                      "SHORT $ ",         "",         "",         ""  };

