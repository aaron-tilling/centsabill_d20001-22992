/*---------------------------------------------------------------------------\
|  Src File:   transv28.c                                                    |
|  Authored:   12/01/93, rz, cj                                              |
|  Function:   Transaction processing.                                       |
|  Comments:                                                                 |
|                                                                            |
|  Revision:                                                                 |
|      1.00:   01/18/95, tgh  -  changed header and filename.                |
|                             -  Removed configuration block references.     |
|      1.02    05/17/95, sjb  -  modify stored value transaction             |
|      1.03:   04/08/98, sjb  -  modify processing dail light savings time.  |
|      1.04:   10/05/98, sjb  -  modify time stamp structure                 |
|      1.05:   05/07/99, sjb  -  modify transfer transaction                 |
|      1.06:   07/27/99, sjb  -  modify transaction for credit cards & cards |
|                                formated like credit cards                  |
|      1.07:   08/17/99, sjb  -  modify transaction for bad list             |
|      1.08:   08/26/99, sjb  -  modify transaction for bad list             |
|      1.09:   08/26/99, sjb  -  modify transaction for bad list             |
|      1.10:   09/21/00, sjb  -  add trim diagnostic transaction             |
|      1.11:   10/05/00, sjb  -  add GPS transaction                         |
|      1.12:   02/28/01, sjb  -  add ticket/token transaction                |
|                             -  add recharge transaction                    |
|      1.13:   03/20/01, aat  -  add TRiM Missfeed and TRiM Card Issued trans|
|      1.14:   06/20/01, sjb  -  add J1708 diagnostic transaction            |
|      1.15:   10/02/01, sjb  -  modify processing restore card transaction  |
|      1.16:   04/16/02, sjb  -  add firmware transaction                    |
|                             -  modifuy magnetic transactions               |
|      1.17:   08/29/02, sjb  -  add driverlogonstatus transaction           |
|      1.19:   11/18/02, sjb  -  modify driverlogonstatus transaction for    |
|                                J1708                                       |
|      1.20:   12/18/02, sjb  -  clear  P_RESTORE_SC flag when restore       |
|                                trasaction saved                            |
|      1.21:   04/14/03, sjb  -  add J1708 date/time error transaction       |
|      1.22:   05/28/03, sjb  -  remove worble from T_clear                  |
|      1.23:   07/29/03, sjb  -  add debug to Powerup & Generic transactions |
|                             -  modify Generic transaction (T_Event) to get |
|                                time_t from data passed to event (*s) if    |
|                                event (Etype) was power loss (FBXPWRL)      |                             
|      1.25:   10/30/03, sjb  -  tweek saving J1708 diag transaction         |
|      1.26:   12/23/03, sjb  -  add driver login via card transaction       |
|      1.27:   09/15/04, sjb  -  tweek display of trim diagnostic transaction|
|                                when in TRANS_Debug                         |
|      1.28:   10/07/04, sjb  -  add group/designator to Special pass        |
|                                transaction                                 |
|           Copyright (c) 1994-2001 GFI All Rights Reserved                  |
\---------------------------------------------------------------------------*/
/* System headers
*/
#include <stdio.h>
#include <string.h>
#include <time.h>

/* Project headers
*/
#include "gen.h"
#include "sci.h"
#include "rtc.h"
#include "regs.h"
#include "magcard.h"
#include "misc.h"
#include "dbase.h"
#include "timers.h"
#include "status.h"
#include "ability.h"
#include "transact.h"
#include "sound.h"
#include "rs485.h"
#include "mag.h"
#include "trim.h"
#include "cnf.h"
#include "j1708.h"
#include "scmd.h"
#include "fbd.h"
#include "process.h"
#include "probe.h"
#include "menu.h"

#define  SEQ_NUM()   (SeqNo = (SeqNo < 255) ? SeqNo+1 : 0)

/* Global data
*/
int    TRANS_Debug;                             /* trans. debug */

/* Module data
*/
static int  SeqNo;                              /* trans. sequence no. */


/* Prototype(s)
*/
time_t  *TimeStamp   ( void*, int );



/* Initialize database
*/
void  T_init()
{
   int   sig_clr = DBinit();                    /* test data base signature */
                                                /* and re-initialize if needed*/

   if( sig_clr )                                /* database re-initialized?   */ 
   {
      memset( (char*)&Mlist, 0, sizeof(T_MasterList) );
      T_event( TRMMEMC, NULL, 0 );              /* log mem (database) cleared */ 
   }

   Mlist.TransactionType   = TC_MasterList;
   Mlist.Config.AgencyCode = MyAgency;
   Mlist.Config.PT_ver     = 1;                 /*fixme*/
   Mlist.Config.FT_ver     = Dlist.FareTblVer;
   Mlist.Config.BL_ver     = Dlist.BL_ver;
   Mlist.Config.BR_ver     = Dlist.BR_ver;
   Mlist.Config.ML_ver     = Dlist.ML_ver;
   Mlist.Config.BD_ver     = 1;                 /*fixme*/
   Mlist.Config.HL_ver     = Dlist.HL_ver;
   Mlist.Config.TC_ver     = Dlist.TC_ver;
   Mlist.SeqNum            = 2;                 /* always 2 */
   memset( (char*)&J1708Diag, 0, sizeof(J1708DIAG) );
}


/*
** Closes out transactions to get ready for probing.
*/
void T_init_probe( void )
{
   T_time_stamp();
}


/*
** Transaction clear. Called after a probe.
*/
void T_clear( void )
{
   DBreinit();

   SeqNo = 2;

   time( &Mlist.Config.LastProbe );

   Mlist.Cumulative.ProbeCnt++;         /* bump up the probe count   */

   memset( (void*)&Mlist.System, 0, sizeof(Mlist.System) );
   memset( (void*)&Mlist.Maintenance, 0, sizeof(Mlist.Maintenance) );

   Mlist.TransactionType   = TC_MasterList;
   Mlist.Config.AgencyCode = MyAgency;
   Mlist.Config.TrimVer    = Version;
   Mlist.Config.PT_ver     = 1;                 /*fixme*/
   Mlist.Config.FT_ver     = Dlist.FareTblVer;
   Mlist.Config.BL_ver     = Dlist.BL_ver;
   Mlist.Config.BR_ver     = Dlist.BR_ver;
   Mlist.Config.ML_ver     = Dlist.ML_ver;
   Mlist.Config.BD_ver     = 1;                 /*fixme*/
   Mlist.Config.HL_ver     = Dlist.HL_ver;
   Mlist.Config.TC_ver     = Dlist.TC_ver;
   Mlist.SeqNum            = 2;                 /* always 2 */
   memset( (char*)&J1708Diag, 0, sizeof(J1708DIAG) );

   init_pbq( 1 );

   dprintf(0, 0, "Complete");
   Set_Sflag( S_COMPLETEPENDING );
}


/*
** Transaction clear. Called after a probe via farebox.
*/
void T_FbxClear( fs_stat )
int              fs_stat;
{
   DBreinit();
   SeqNo = 2;
}

/****************************************************************************
** T_power_up ()
** Transaction Type 01h
** Usage: at power up
****************************************************************************/

void T_power_up( void )
{
   int          i,j;

   T_PowerUp   t;
   memset( &t, 0, sizeof(t) );

   TimeStamp( &t, TC_PowerUp );
   t.Code   = rsr.w;
   DBadd( (void*)&t, (int)sizeof(t) );

   if ( TRANS_Debug )
   {
     j = sizeof(T_PowerUp);
     printf("\nPower up Transaction\n");
     for( i = 0; i < j; i++ )
        printf("%02x ", ((byte*)(void*)&t)[i] );
     printf("\n");
   }
}

/****************************************************************************
** Transaction Type 02h
** Usage: Once at midnight (called from T_time_stamp)
****************************************************************************/
void T_sync( void )
{
   T_Sync      t;

   TimeStamp( &t, TC_Sync );
   DBadd( (void*)&t, (int)sizeof(t) );
}

/****************************************************************************
** Transaction Type 03h
** Usage: Indicates the top of the hour. Also used to stamp the time when 
**        probing started and completed.
****************************************************************************/

void T_time_stamp( void )
{
   T_TimeStamp t;

   TimeStamp( &t, TC_TimeStamp );

   DBadd( (void*)&t, (int)sizeof(t) ); 
}


/****************************************************************************
** Type 04h/33H
** Usage: When the driver changes
****************************************************************************/

void T_driver_change( ulong dvr )
{
   int         i,j;
   T_Driver    t;

   if ( DriverEvent >= TC_DriverSwipe && DriverEvent <= TC_DriverJ1708 )
      TimeStamp( &t, DriverEvent );
   else
      TimeStamp( &t, TC_Driver );

   t.NewDrvr= dvr;
   DBadd( (void*)&t, (int)sizeof(t) );
   if ( TRANS_Debug )
   {
     j = sizeof(T_Driver);
     printf("\nDriver Change Transaction\n");
     for( i = 0; i < j; i++ )
        printf("%02x ", ((byte*)(void*)&t)[i] );
     printf("\n");
   }
}


/****************************************************************************
** Type 05h
** Usage: When the fareset changes
** fixme: This may need to be stored immediately after T_PowerUp.
****************************************************************************/

void T_fareset_change( byte fs )
{
   T_Fareset  t;

   TimeStamp( &t, TC_Fareset );
   t.NewFs  = fs;
   DBadd( (void*)&t, (int)sizeof(t) );
}


/****************************************************************************
** Type 06h
** Usage: When the route changes
****************************************************************************/

void T_route_change( ulong rt )
{
   T_Route t;

   TimeStamp( &t, TC_Route );
   t.NewRt  = rt;
   DBadd( (void*)&t, (int)sizeof(t) );
}

/****************************************************************************
** Type 07h
** Usage: When the run changes
****************************************************************************/

void T_run_change( ulong run )
{
   T_Run   t;

   TimeStamp( &t, TC_Run );
   t.NewRun = run;
   DBadd( (void*)&t, (int)sizeof(t) );
}


/****************************************************************************
** Type 08h
** Usage: When the trip changes
****************************************************************************/

void T_trip_change( ulong tp )
{
   T_Trip  t;

   TimeStamp( &t, TC_Trip );
   t.NewTp  = tp;
   DBadd( (void*)&t, (int)sizeof(t) );
}


/****************************************************************************
** Type 09h
** Usage: When the direction changes
****************************************************************************/

void T_direction_change( byte dir )
{
   int          i,j;
   T_Direction  t;

   memset( &t, 0, sizeof(t) );

   TimeStamp( &t, TC_Direction );
   t.NewDir = dir;
   if( t.NewDir )
     --t.NewDir; 
   DBadd( (void*)&t, (int)sizeof(t) );

   if ( TRANS_Debug )
   {
     j = sizeof(T_Direction);
     printf("New Direction\n");
     for( i = 0; i < j; i++ )
        printf("%02x ", ((byte*)(void*)&t)[i] );
     printf("\n");
   }
}


/****************************************************************************
** Type 0ah
** Usage: When the stop level changes
****************************************************************************/

void T_stop_change( ulong st )
{
   int     i,j;
   T_Stop  t;

   memset( &t, 0, sizeof(t) );

   TimeStamp( &t, TC_Stop );
   t.NewST  = st;
   DBadd( (void*)&t, (int)sizeof(t) );

   if ( TRANS_Debug )
   {
     j = sizeof(T_Stop);
     printf("\nStop Transaction\n");
     for( i = 0; i < j; i++ )
        printf("%02x ", ((byte*)(void*)&t)[i] );
     printf("\n");
   }
}


/****************************************************************************
** Type 0bh
** Usage: When the cut time changes
****************************************************************************/


void T_cut_change( byte hrs, byte min )
{
   T_Cut  t;

   TimeStamp( &t, TC_Cut );
   t.Cut_H  = hrs;
   t.Cut_M  = min;
   DBadd( (void*)&t, (int)sizeof(t) );
}


/****************************************************************************
** Type 0ch
** Usage: When the driver enters all data necessary to be logged in.
**        
****************************************************************************/

void T_ready_for_revenue( void )
{
   T_ReadyForRevenue    t;

   TimeStamp( &t, TC_ReadyForRevenue );
   DBadd( (void*)&t, (int)sizeof(t) );
}


/****************************************************************************
** Type 0dh
** Usage: Whenever a maintenance code is entered
****************************************************************************/

void T_maintenance_code( word code )
{
   T_MaintenanceCode    t;

   TimeStamp( &t, TC_MaintenanceCode );
   t.Code   = code;
   DBadd( (void*)&t, (int)sizeof(t) );
}


/****************************************************************************
** Type 0eh
** Usage: Whenever a stored ride card is processed.
****************************************************************************/

void  T_Stored_Ride( void *card )
{
   int            i,j;
   T_StoredRide   t;

   memset( &t, 0, sizeof(t) );

   TimeStamp( &t, TC_StoredRide );
   t.RemVal    = ((CARD*)card)->RemVal;
   t.Restored  = 0;
   t.TTP_used  = (byte)(((CARD*)card)->TTP_used);
   t.F_Use     = ((CARD*)card)->FirstUse;
   t.ImpValue  = ((CARD*)card)->XferVal;
   memcpy( (byte*)&t.SerialNo, ((CARD*)card)->serial+2, sizeof( SERIAL ) );

   DBadd( (void*)&t, (int)sizeof(t) );

   if ( TRANS_Debug )
   {
     j = sizeof(T_StoredRide);
     printf("\nStored Ride Transaction\n");
     for( i = 0; i < j; i++ )
        printf("%02x ", ((byte*)(void*)&t)[i] );
     printf("\n");
   }
}


/****************************************************************************
** Type 0fh
** Usage: Whenever a period pass is processed.
****************************************************************************/

void T_period_pass( void *card, word flags )
{
   int             i,j;
   TS_PeriodPass   t;

   memset( &t, 0, sizeof(t) );

   TimeStamp( &t, TC_PeriodPass );
   t.mf = flags;
   if ( ((CARD*)card)->FirstUse )
     t.mf |= mf_FIRSTUSE;

   memcpy( (byte*)&t.sn, ((CARD*)card)->serial+2, sizeof( SERIAL ) );
   DBadd( (void*)&t, (int)sizeof(t) );

   if ( TRANS_Debug )
   {
     j = sizeof(TS_PeriodPass);
     printf("\nPeriod Transaction\n");
     for( i = 0; i < j; i++ )
        printf("%02x ", ((byte*)(void*)&t)[i] );
     printf("\n");
   }
}

/****************************************************************************
** Type 10h
** Usage: Whenever a stored value card is processed.
****************************************************************************/

void T_Stored_Value( void *card )
{
   int            i,j;
   T_StoredValue  t;

   memset( &t, 0, sizeof(t) );

   TimeStamp( &t, TC_StoredValue );
   memcpy( (byte*)&t.SerialNo, ((CARD*)card)->serial+2, sizeof( SERIAL ) );
   t.FareDeducted = ((CARD*)card)->FareDeducted;
   t.RemVal       = ((CARD*)card)->RemVal;
   t.Restored     = 0;
   t.F_Use        = ((CARD*)card)->FirstUse;
   t.KTFare       = (byte)((CARD*)card)->KTFare;  /* index of card in fare table */ 
   t.TTP_used     = (byte)((CARD*)card)->TTP_used; /* what card paid for */
   t.Fareset      = FbxCnf.fareset;
   t.ImpValue     = ((CARD*)card)->XferVal;
   DBadd( (void*)&t, (int)sizeof(t) );

   if ( TRANS_Debug )
   {
     j = sizeof(T_StoredValue);
     printf("\nStored Value Transaction\n");
     for( i = 0; i < j; i++ )
        printf("%02x ", ((byte*)(void*)&t)[i] );
     printf("\n");
   }
}

/****************************************************************************
** Type 11h
** Usage: Whenever a credit card is processed.
****************************************************************************/

void T_credit_card( void *serial, void *fare )
{
   int           i,j;
   T_CreditCard  t;

   memset( &t, 0, sizeof(t) );

   TimeStamp( &t, TC_CreditCard );
   memcpy( (void*)&t.SerialNo, serial, CCSZ );
   t.FareDeducted = ((T_FARE*)fare)->FareDeducted;
   t.KTFare       = ((T_FARE*)fare)->TTPFareIdx;  /* index of card in fare table */
   t.TTP_used     = ((T_FARE*)fare)->TTPUsed;     /* what card paid for */
   t.Fareset      = FbxCnf.fareset;
   DBadd( (void*)&t, (int)sizeof(t) );

   j = sizeof(T_CreditCard);
   if ( TRANS_Debug )
   {   
      printf("\nCredit/Smart Transaction\n");
      for( i = 0; i < j; i++ )
         printf("%02x ", ((byte*)(void*)&t)[i] );
      printf("\n");
   }
}

/****************************************************************************
** Type 11h
** Usage: Whenever a credit card is processed.
****************************************************************************/

void T_Credit_Card( void *card )
{
   int           i,j;
   T_CreditCard  t;

   memset( &t, 0, sizeof(t) );

   TimeStamp( &t, TC_CreditCard );
   memcpy( (void*)&t.SerialNo, ((CARD*)card)->cc_serial, CCSZ );
   t.FareDeducted = ((CARD*)card)->FareDeducted;
   t.KTFare       = ((CARD*)card)->KTFare;  /* index of card in fare table */
   t.TTP_used     = ((CARD*)card)->TTP_used;  /* what card paid for */
   t.Fareset      = FbxCnf.fareset;
   DBadd( (void*)&t, (int)sizeof(t) );

   j = sizeof(T_CreditCard);
   if ( TRANS_Debug )
   {   
      printf("\nCredit/Smart Transaction\n");
      for( i = 0; i < j; i++ )
         printf("%02x ", ((byte*)(void*)&t)[i] );
      printf("\n");
   }
}

/****************************************************************************
** Type 12h
** Usage: Whenever a magnetic pass transfer is processed.
****************************************************************************/

void T_Receive_Transfer( void *card )
{
   int                   i,j;
   TS_ReceiveTransfer    t;

   memset( &t, 0, sizeof(t) );

   TimeStamp( &t, TC_ReceiveTransfer );
   memcpy( (byte*)&t.SerialNo, ((CARD*)card)->serial+2, sizeof( SERIAL ) );
   t.OrigRoute  = ((CARD*)card)->IURoute;
   t.OrigDir    = ((CARD*)card)->IUDir;
   t.Restored   = 0;
   t.flags      = ((CARD*)card)->xflags;
   t.ImpValue   = ((CARD*)card)->XferVal;
   DBadd( (void*)&t, (int)sizeof(t) );

   if ( TRANS_Debug )
   {
     j = sizeof(TS_ReceiveTransfer);
     printf("\nTransfer Transaction\n");
     for( i = 0; i < j; i++ )
        printf("%02x ", ((byte*)(void*)&t)[i] );
     printf("\n");
   }
}


/****************************************************************************
** Type 13h
** Usage: When the current fare is satisified.
****************************************************************************/

void T_got_fare( void *fare )
{
   int                   i,j;
   T_GotFare   t;

   memset( &t, 0, sizeof(t) );

   TimeStamp( &t, TC_GotFare );
   t.Key        = ((T_FARE*)fare)->Key;
   t.Value      = ((T_FARE*)fare)->ImpValue;
   t.Flag       = ((T_FARE*)fare)->Flags;

   DBadd( (void*)&t, (int)sizeof(t) );

   if ( TRANS_Debug )
   {
     j = sizeof(T_GotFare);
     printf("\nKey Got Fare Transaction\n");
     for( i = 0; i < j; i++ )
        printf("%02x ", ((byte*)(void*)&t)[i] );
     printf("\n");
   }
}


/****************************************************************************
** Type 14h
****************************************************************************/

void T_bypass_dmp( void *fare )
{
   T_BypassDmp    t;

   TimeStamp( &t, TC_BypassDmp );
   t.Revenue = ((T_FARE*)fare)->Revenue;
   DBadd( (void*)&t, (int)sizeof(t) );
}


/****************************************************************************
** Type 15h
****************************************************************************/

void T_escrow_dmp( void *fare )
{
   T_EscrowDmp    t;

   TimeStamp( &t, TC_EscrowDmp );
   t.Revenue    = ((T_FARE*)fare)->Revenue;
   DBadd( (void*)&t, (int)sizeof(t) );
}


/****************************************************************************
** Type 16h
****************************************************************************/

void T_escrow_tmo( void *fare )
{
   T_EscrowTmo    t;

   TimeStamp( &t, TC_EscrowTmo );
   t.Revenue = ((T_FARE*)fare)->Revenue;
   DBadd( (void*)&t, (int)sizeof(t) );
}


/****************************************************************************
** Type 17h
** Usage: For GFI Debugging
****************************************************************************/

void T_gpt( int type, byte *s, int sz )
{
   int       i,j;
   T_GPT       t;

   memset( &t, 0, sizeof(t) );
   TimeStamp( &t, TC_GPT );
   t.type = type;
   memcpy( &t.String, s, sz );
   DBadd( (void*)&t, (int)sizeof(t) );

   if ( TRANS_Debug )
   {
     printf("ORBITAL TRANSACTION ");
     j = sizeof(T_GPT);
     for( i = 0; i < j; i++ )
        printf("%02x ", ((byte*)(void*)&t)[i] );
     printf("\n");
   }
}


/****************************************************************************
** Type 18h
** Usage: Whenever a bad listed card is detected.
****************************************************************************/

void T_badcard( void *serial )
{
   TS_BadCard   t;

   if ( Clist2.mf & mf_BADLIST )
   {
      TimeStamp( &t, TC_BadCard );
      memcpy( (byte*)&t.SerialNo, (byte*)serial, sizeof(B_SERIAL) );
      t.F_Use   = 0;
      t.pad     = 0;
      DBadd( (void*)&t, (int)sizeof(t) );
   }
}


/****************************************************************************
** Type 19h
** Usage: Whenever a person logs in.
****************************************************************************/

void T_log_in( SERIAL_F2 *serial, int manual )
{
   T_LogIn     t;

   TimeStamp( &t, TC_LogIn );
   memcpy( (byte*)&t.SerialNo, (byte*)serial, sizeof(SERIAL_F2) );
   t.Manual = manual;
   t.pad    = 0;

   DBadd( (void*)&t, (int)sizeof(t) );
}


/****************************************************************************
** Type 1ah
** Usage: Whenever a magnetic card has been restored to its previous state
****************************************************************************/

int T_restored_card( void *serial )
{
   int       i,j;
   T_RestoreCard  t;

   TimeStamp( &t, TC_RestoreCard );
   memcpy( (byte*)&t.SerialNo, (byte*)serial+2, sizeof(SERIAL) );
   if ( db_restore_card( &t.SerialNo ))
   {
      DBadd( (void*)&t, (int)sizeof(t) );

      if ( TRANS_Debug )
      {
         printf("restored TRANSACTION\n ");
         j = sizeof(T_GPT);
         for( i = 0; i < j; i++ )
            printf("%02x ", ((byte*)(void*)&t)[i] );
         printf("\n");
      }
      i = TRUE;
      delete_pbq( serial );
   }
   else
   {
      i = FALSE;
   }
   Clr_Pflag( P_RESTORE_SC );
   return i;
}


/****************************************************************************
** Type 1bh
** Usage: Whenever the read function of the farebox is performed
****************************************************************************/

void T_read_card( void *serial )
{
   T_ReadCard  t;

   TimeStamp( &t, TC_ReadCard );
   memcpy( (byte*)&t.SerialNo, (byte*)serial+2, sizeof(SERIAL) );

   DBadd( (void*)&t, (int)sizeof(t) );
}


/****************************************************************************
** Type 1ch
** Usage: Whenever a non-imbedded transfer is issued
****************************************************************************/

void T_issue_transfer( void *serial, int tty )
{
   T_IssueTransfer   t;

   memset( &t, 0, sizeof(t) );

   TimeStamp( &t, TC_IssueTransfer );
   memcpy( (byte*)&t.SerialNo, (byte*)serial+2, sizeof(SERIAL) );
   t.XferIssue =  tty;

   DBadd( (void*)&t, (int)sizeof(t) );

}


/****************************************************************************
** Type 1dh
** Usage: Whenever a period pass is processed.
****************************************************************************/

void T_Special_Pass( void *card, word flags )
{
   int                i,j;
   TS_Special_Period  t;

   memset( &t, 0, sizeof(t) );

   TimeStamp( &t, TC_Special_Period );
   t.mf = flags;
   t.gd = CREDIT_MG<<5|((CARD*)card)->Desig;
   memcpy( (void*)&t.sn, ((CARD*)card)->cc_serial, CCSZ );
   DBadd( (void*)&t, (int)sizeof(t) );

   if ( TRANS_Debug )
   {
     printf("SPECIAL CARD TRANSACTION\n ");
     j = sizeof( TS_Special_Period );
     for( i = 0; i < j; i++ )
        printf("%02x ", ((byte*)(void*)&t)[i] );
     printf("\n");
   }

}

/****************************************************************************
** Type 1eh
** Usage: When a cashbox is inserted into a FareBox
**        or when a cashbox is removed from a FareBox
****************************************************************************/

void T_cash_box( word id, byte code )
{
   T_CashBox   t;

   TimeStamp( &t, TC_CashBox );

   t.CbxID  = id;
   t.Type   = code;
   DBadd( (void*)&t, (int)sizeof(t) );
}


/****************************************************************************
** Type 1fh
** Usage: When an electronic key is used to open a FareBox
**        The keynum is a two digit serial # (00-99)
****************************************************************************/

void T_electronic_key( byte key )
{
   T_ElectronicKey   t;

   TimeStamp( &t, TC_ElectronicKey );
   t.keynum  = key;
   DBadd( (void*)&t, (int)sizeof(t) );
}


/****************************************************************************
** Type 20h
** Usage: When a FareBox or TRiM is powered on and
**        performs the self test procedures.
**        This transactions contains the DeltaT time
**        when the unit lost power.
****************************************************************************/

void T_post( byte Hour, byte Min, byte flag )
{
   T_POST   t;
   memset( &t, 0, sizeof(t) );

   TimeStamp( &t, TC_POST );
   t.Flags  = flag;
   DBadd( (void*)&t, (int)sizeof(t) );
}


/****************************************************************************
** Type 23h
** Usage: When cash is added to stored value card
****************************************************************************/
void T_Cashadded( void *card, word cash )
{
   int     i,j;
   TS_MetroCash  t;

   memset( &t, 0, sizeof(t) );

   TimeStamp( &t, TC_MetroCash );
   t.value     = cash;
   t.new_val   = ((CARD*)card)->RemVal;
   t.pay_meth  = CASH_PAY;
   t.authorize = FbxCnf.driver;
   memcpy( (byte*)&t.SerialNo, ((CARD*)card)->serial+2, sizeof(SERIAL) );
   DBadd( (void*)&t, (int)sizeof(t) );

   if ( TRANS_Debug )
   {
       j= sizeof(TS_MetroCash);
       printf("\ncash added Transaction\n");
       for( i=0; i<j; i++ )
           printf("%02x ", ((byte*)(void*)&t)[i] );
       printf("\n");
   }
}

/****************************************************************************
** Type 24h
** Usage: trim diagnostic transaction
**
****************************************************************************/
void T_trim_diag( void *diag )
{
   int     i,j;
   T_TrimDiag  t;

   memset( &t, 0, sizeof(t) );

   TimeStamp( &t, TC_TrimDiag );
   memcpy( (byte*)&t.id, (byte*)diag, sizeof( TRIM_DIAG ) );
   if ( t.version >= 181 )
      memcpy( (byte*)&t.powerdown, (byte*)Trim_EDiag.powerdown,
         sizeof( TRIM_EDIAG ) );

   if ( TRANS_Debug )
   {
     j = sizeof(T_TrimDiag);
     printf("\nTRIM Diag Transaction\n");
     for( i = 0; i < j; i++ )
        printf("%02x ", ((byte*)(void*)&t)[i] );
     printf("\n");
   }

   DBadd( (void*)&t, (int)sizeof(t) );
}

/****************************************************************************
** Type 25h
** Usage: trim diagnostic transaction
**
****************************************************************************/
void T_bad_verify( void *data, int n )
{
   int       i,j;
   T_BadVerify  t;

   memset( &t, 0, sizeof(t) );

   TimeStamp( &t, TC_TrimBadVerify );
   memcpy( (byte*)&t.ver, data, n );
   DBadd( (void*)&t, (int)sizeof(t) );

   if ( TRANS_Debug )
   {
     j = sizeof(T_BadVerify);
     printf("\nTransaction\n");
     for( i = 0; i < j; i++ )
        printf("%02x ", ((byte*)(void*)&t)[i] );
     printf("\n");
   }

}


/****************************************************************************
** Type 26h
** Usage: GPS position transaction
**
****************************************************************************/
void T_Gps( void *data, int sz )
{
   int       i,j;
   T_GPS     t;

   memset( &t, 0, sizeof(t) );

   TimeStamp( &t, TC_GPS );

   memcpy( (byte*)&t.Lat, data, sz );
   DBadd( (void*)&t, (int)sizeof(t) );

   if ( TRANS_Debug )
   {
     j = sizeof(T_GPS);
     printf("\nGPS Transaction\n");
     for( i = 0; i < j; i++ )
        printf("%02x ", ((byte*)(void*)&t)[i] );
     printf("\n");
   }

}

/****************************************************************************
** Transaction Type 29h
** Usage: Indicates trim misfeed  
**        
****************************************************************************/

void T_misfeed( void )
{
   int          i,j;
   T_MisFeed  t;

   memset( &t, 0, sizeof(t) );

   TimeStamp( &t, TC_Misfeed );

   DBadd( (void*)&t, (int)sizeof(t) ); 
   if ( TRANS_Debug )
   {
     j = sizeof(T_MisFeed);
     printf("\nmisfeed transaction\n");
     for( i = 0; i < j; i++ )
        printf("%02x ", ((byte*)(void*)&t)[i] );
     printf("\n");
   }

}

/****************************************************************************
** Type 2ah
** Usage: card issued transaction ( non-transfer )
**
****************************************************************************/
void T_issue_card( void *data )
{
   int          i,j;
   T_IssueCard  t;

   memset( &t, 0, sizeof(t) );

   TimeStamp( &t, TC_IssuedCard );
   memcpy( (byte*)&t.SerialNo, data, sizeof(TRIM_ISSUE_PASS));

   DBadd( (void*)&t, (int)sizeof(t) );

   if ( TRANS_Debug )
   {
     j = sizeof(T_IssueCard);
     printf("\ncard issued Transaction\n");
     for( i = 0; i < j; i++ )
        printf("%02x ", ((byte*)(void*)&t)[i] );
     printf("\n");
   }
}
   
/****************************************************************************
** Type 2bh
** Usage: To record a single TTP transaction for each ticket or token 
**        that has been dropped into and correctly catagorized by 
**        the farebox. 
**
****************************************************************************/

void T_ticket_token( int ttp, int val )
{

   int       i,j;
   T_TTP     t;

   memset( &t, 0, sizeof(t) );

   TimeStamp( &t, TC_TokenTicket );

   t.TTPFareId = ttp;
   t.ModifyRev = val;

   DBadd( (void*)&t, (int)sizeof(t) );

   if ( TRANS_Debug )
   {
     j = sizeof(T_TTP);
     printf("\nTTP Transaction\n");
     for( i = 0; i < j; i++ )
        printf("%02x ", ((byte*)(void*)&t)[i] );
     printf("\n");
   }
}

/****************************************************************************
** Transaction Type 2ch
** Usage: Indicates bill validator OOS (ODYSSEY ONLY ) 
**        
****************************************************************************/

/****************************************************************************
** Transaction Type 2dh
** Usage: La metrocard group transaction (LA ONLY ) 
**        
****************************************************************************/

/****************************************************************************
** Type 2eh
** Usage: J1708 diagnostic transaction
**
****************************************************************************/
void T_J1708_diag( void *data )
{
   int              i,j;
   T_J1708Diag      t;

   memset( &t, 0, sizeof(t) );

   TimeStamp( &t, TC_J1708_DIAG );
/*
   t.message_cnt     = J1708MessCnt;
   t.lrc_err         = J1708LrcErr;
   t.timeout         = J1708ErrCnt;
   t.collision       = J1708Collision;
   t.response        = J1708RespErr;
*/
   ((J1708DIAG*)data)->J1708lat  = FbxCnf.latitude;
   ((J1708DIAG*)data)->J1708long = FbxCnf.longitude;
   ((J1708DIAG*)data)->J1708stop = FbxCnf.stop;
   ((J1708DIAG*)data)->J1708Poll   = Clist2.J1708Poll;
   ((J1708DIAG*)data)->J1708Pid502 = Clist2.logon;
   ((J1708DIAG*)data)->J1708Config = Clist2.OptionFlags3;
   ((J1708DIAG*)data)->J1708IdleFg  = Clist2.hourlyevent;

   memcpy( (byte*)&t.data, data, sizeof(J1708DIAG));

   DBadd( (void*)&t, (int)sizeof(t) ); 
   if ( TRANS_Debug )
   {
      j = sizeof( T_J1708Diag );
      printf("\nJ1708 transaction\n");
      for( i = 0; i < j; i++ )
         printf("%02x ", ((byte*)(void*)&t)[i] );
      printf("\n");
   }
}

/****************************************************************************
** Type 2fh
** Usage: Firmware transaction
**
****************************************************************************/
void T_Firmware_Info( void )
{
   int              i,j;
   T_Firmware       t;

   memset( &t, 0, sizeof(t) );

   TimeStamp( &t, TC_Firmware );

   t.BusNum = FbxCnf.busno;                 /* Bus Number                    */
   t.FareBoxNum = FbxCnf.fbxno;             /* Farebox Serial Number         */
   t.FareBoxPart = Clist.PRIVATE.PartNo;    /* Farebox Firmware Base         */
   t.FareBoxVer = Ml.fbx_v;                 /* Farebox Firmware Version      */
   t.TrimNum = Ml.trim_n;                   /* Trim Serial Number            */
   t.TrimPart;                              /* Trim Firmware Base            */
   t.TrimVer = Ml.trim_v;                   /* Trim Firmware Version         */

/*   t.OCUNum;                                /* OCU Serial Number             */
/*   t.OCUPart = OCU_Part_No;                 /* OCU Firmware Base             */
/*   t.OCUVer = OCU_ver;                      /* OCU Firmware Version          */

   t.SmartCardNum;                          /* SmartCard Serial Number       */
   t.SmartCardPart = SSC_base;              /* SmartCard Firmware Base       */
   t.SmartCardVer = SSC_ver;                /* SmartCard Firmware Version    */
   t.mux_ver = Prb_Req_Parms.ver;           /* mux version   */

   DBadd( (void*)&t, (int)sizeof(t) ); 
   if ( TRANS_Debug )
   {
      j = sizeof( T_Firmware );
      printf("\nFirmware Transaction\n");
      for( i = 0; i < j; i++ )
         printf("%02x ", ((byte*)(void*)&t)[i] );
      printf("\n");
   }
}

/****************************************************************************
** Type 30h
** Usage: J1708 Driver logon status transaction
**
****************************************************************************/
void T_J1708DriverStat( void )
{
   int              i,j;
   T_J1708DvrStat   t;

   memset( &t, 0, sizeof(t) );

   TimeStamp( &t, TC_J1708DvrStat );
   t.status = DriverLogonStatus;

   DBadd( (void*)&t, (int)sizeof(t) ); 

   if ( TRANS_Debug )
   {
      j = sizeof( T_J1708DvrStat );
      printf("\nJ1708 Driver Status\n");
      for( i = 0; i < j; i++ )
         printf("%02x ", ((byte*)(void*)&t)[i] );
      printf("\n");
   }
 
}

/****************************************************************************
** Type 32h
** Usage: When time discrepancy detected from J1708
****************************************************************************/

void T_J1708_tm_err( byte *p, byte type, ulong diff )
{
   int              i,j;
   T_J1708TIME_ERR    t;

   memset( &t, 0, sizeof(t) );

   TimeStamp( &t, TC_J1708_TM_ERR );

   t.type = type;
   t.sec  = *p;        /* seconds or day of month ( in 1/4 units ) */
   t.min  = *(p+1);    /* minutes or month months */
   t.hour = *(p+2);    /* hours or years          */

   t.diff = diff;
   
   DBadd( (void*)&t, (int)sizeof(t) );
   if( TRANS_Debug )
   {
      j = sizeof( T_J1708TIME_ERR );
      printf("\nJ1708 time error\n");
      for( i = 0; i < j; i++ )
         printf("%02x ", ((byte*)(void*)&t)[i] );
      printf("\n");
   }

}

/****************************************************************************
** Type 37h
** Usage: Whenever a maintenance pass is entered
****************************************************************************/

void T_maintenance_pass( void *sn )
{
   int         i,j;
   T_Maintenance_Pass   t;

   memset( &t, 0, sizeof(t) );

   TimeStamp( &t, TC_Maintenance_Pass );
   memcpy( (byte*)&t.sn, (byte*)sn+2, sizeof( SERIAL ) );
   DBadd( (void*)&t, (int)sizeof(t) );

   if( TRANS_Debug )
   {
      j = sizeof( T_Maintenance_Pass );
      printf("\nProbe enabled\n");
      for( i = 0; i < j; i++ )
         printf("%02x ", ((byte*)(void*)&t)[i] );
      printf("\n");
   }

}


/****************************************************************************
** Type 80h
** Usage: When an event occurs.
****************************************************************************/

void T_event( byte event, char *s, int n )
{
   int         i,j;
   T_Event     t;

   TimeStamp( &t, TC_Event );
   t.Ctype  = 2;
   t.Etype  = event;
   memset( t.dat, ' ', sizeof(t.dat) );
   if ( event != FBXPWRL )
   {
      if ( s != NULL && (0<n && n<=sizeof(t.dat)) )
         memcpy( t.dat, s, n );
   }
   else if ( n >= 4 )
   {
      t.h.t = *(long*)s;
   }   
   t.null   = 0;
   t.flag   = 0;

   DBadd( (void*)&t, (int)sizeof(t) );

   if ( TRANS_Debug )
   {
     j = sizeof( T_Event );
     printf("\nGeneric Transaction\n");
     for( i = 0; i < j; i++ )
        printf("%02x ", ((byte*)(void*)&t)[i] );
     printf("\n");
   }
}

void  T_FbxRec( void *p, int n )
{
   T_GENERIC   t;

   if ( n < 132 )
   {
      memcpy( &t, p, n );
      TimeStamp( &t, t.TType );
      DBadd( (void*)&t, n );
   }
}

/* Time stamp transactions which use
   the standard header format.
*/
static
time_t  *TimeStamp( p, type )
void                  *p;
int                       type;
{
   time_t      t;
   struct hdr
   {
      byte   type;                               /* Transaction Type       */
      byte   seq;                                /* sequence number        */
      time_t t;   
   } *m = (struct hdr*)p;

   m->type  = type;
   m->seq   = SEQ_NUM();
   m->t     = time( &t );

   return &t;
}
