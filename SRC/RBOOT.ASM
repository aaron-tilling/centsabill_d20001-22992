*-----------------------------------------------------------------------------+
*  Src File:   rboot01.asm                                                    |
*  Authored:   08/21/97, tgh                                                  |
*  Function:   reprogram boot block.                                          |
*  Comments:                                                                  |
*                                                                             |
*  Revision:                                                                  |
*      1.00:   06/05/01, sjb  -  Initial release.                             |
*                                                                             |
*           Copyright (c) 2001        GFI/USPS All Rights Reserved            |
*-----------------------------------------------------------------------------+

*-----------------------------------------------------------------------------+
*                       Include Files                                         |
*-----------------------------------------------------------------------------+
        include gen.inc
        include fpga.inc
        include regs.inc
        include quart.inc

*-----------------------------------------------------------------------------+
*                       Local Equates                                         |
*-----------------------------------------------------------------------------+

        PCMCIA:         equ     0400000h        ; PCMCIA base address
        PCMCIA_STAT:    equ     0700006h        ; PCMCIA status inputs

        PCMCIA_PGMID:   equ     055aa0001h      ; card holds a program image
        PCMCIA_EXEID:   equ     055aa0002h      ; card holds executable code

        RamLoadAddress: equ     0010e000h       ; relocate code here
        ModuleAddress:  equ     00000000h       ; flash base address

*-----------------------------------------------------------------------------+
*                       Local Macros                                          |
*-----------------------------------------------------------------------------+

*       Service watchdog
        _watchdog:      macro
                        move.b  #055h,swsr      ;; service watchdog
                        move.b  #0aah,swsr
                        endm


*-----------------------------------------------------------------------------+
*                       Data Segment (local data)                             |
*-----------------------------------------------------------------------------+
               section  udata,,"data"           uninitialized data

*-----------------------------------------------------------------------------+
*                       External References                                   |
*-----------------------------------------------------------------------------+

*-----------------------------------------------------------------------------+
*                       Code Segment                                          |
*-----------------------------------------------------------------------------+
                section S_boot_code,,"code"

      xdef  _Boot_Code,_Boot_CodeEnd

*-----------------------------------------------------------------------------+
*                       Flash programming utilities                           |
*                                                                             |
*       NOTE:  Any subroutines placed in this block must be called with       |
*              the "bsr" mnemonic in place of "jsr".                          |
*                                                                             |
*-----------------------------------------------------------------------------+
      _Boot_Code:   equ     *               ; start code

*-----------------------------------------------------------------------------+
*  Function:   BlockEraseAndProgram()                                         |
*  Purpose :                                                                  |
*  Synopsis:   void  BlockEraseAndProgram( void );                            |
*  Input   :                                                                  |
*  Output  :   int - 0(OK) -1(ERR)                                            |
*  Register:                                                                  |
*  Comments:                                                                  |
*-----------------------------------------------------------------------------+
       BlockEraseAndProgram:

                move.l  #0fffh,d1               ; d1 == module length
                movea.l #00402000h,a0           ; a0 -> module data
                bsr     CalcCS
                cmp.l   (a0),d0                 ; compare word
                beq     BootCSOK
                move.b  #03h,led                ; indicate booboo
                bra     ChkWait

        BootCSOK:   
                bsr     PowerOn                 ; turn flash power on
 
                btst.b  #.bit1,porte            ; vpp on ?
                bne.s   continue                ; branch if yes
                bsr     PowerOff
                move.b  #09h,led                ; indicate vpp error
                bra.b   ChkWait

        continue:
                move.b  #081h,led
                movea.l #000000h,a0             ; 16k boot block
                bsr     Erase

                move.b  #085h,led

                move.l  #2000h,d1               ; d1 == module length
                movea.l #00402000h,a0           ; a0 -> module data
                movea.l #00000h,a1              ; a1 -> destination

                bsr     Write                   ; write data to flash

                bsr     PowerOff                ; turn flash power off

                move.l  #1000h,d1               ; d1 == module length
                movea.l #00402000h,a0           ; a0 -> module data
                movea.l #00000h,a1              ; a1 -> destination

                bsr     Verify                  ; verify data in flash
                cmp.w   #0,d0                   ; if it's the same
                beq     BootProgramOK
                move.b  #0bh,led                ; indicate error
                bra.b   ChkWait

           BootProgramOK:
                move.b  #0fh,led                ; indicate ok

           ChkWait:                
                _watchdog
                move.b  PCMCIA_STAT,d0          ; check CD1 & CD2
                and.b   #06h,d0
                beq     ChkWait                 ; card present stay in loop
               
        ExitBlock:
                rts

*---------------------------------------------------------------------------+
*   Function:   CalcCS                                                      |
*   Synopsis:   int CS = CalcDS( p, n );                                    |
*   Input   :   a0 - points to data                                         |
*   Input   :   d1 - holds number of bytes (should be a multiple of 4 ).    |
*   Output  :   d0 will hold checksum                                       |
*   Comments:   local function to calculate a simple 32-bit checksum.       |
*---------------------------------------------------------------------------+
        CalcCS:
                move.l  #0,d0                   ; initialize accumulator
                blo.b   CalcCS_exit
                bra.b   CalcCS02

        CalcCS01:
                add.l   (a0)+,d0
        CalcCS02:
                subq.l  #1,d1                   ; update/check  counter
                bpl.b   CalcCS01

        CalcCS_exit:
                rts

*-----------------------------------------------------------------------------+
*  Function:   PowerOn()                                                      |
*  Purpose :   Local function to turn on programming voltage.                 |
*  Synopsis:   void  PowerOn( void );                                         |
*  Input   :   None.                                                          |
*  Output  :   None.                                                          |
*  Register:                                                                  |
*  Comments:                                                                  |
*-----------------------------------------------------------------------------+
        PowerOn:

                movem.l d0-d7/a0-a6,-(sp)       ;save registers used

                bset.b  #6,porte                ; set WP#, unlock boot block of flash

                move.b  #80h,led                ; set Vpp enable bit in fpga
                move.b  #99h,encdr              ; write 99h to enable shift register in fpga
                move.w  #0a302h,spcr0           ; master, 8 bit xfer, 4.19MHz
                move.w  #0000h,spcr2            ; set end ptr = 0, start ptr = 0
                move.w  #0000h,spcr3            ; QSPI not finished, normal mode
                move.w  #0101h,spcr1            ; disable qspi
                move.b  #02h,qpar               ; set mosi as qspi pin

                move.b  #30h,cram0              ; initialize command ram  
                move.b  #66h,tr01               ; move opposite of enc(0-7)into tran_ram0
                move.w  #0000h,spcr3            ; qspi not finished, normal mode
                move.w  #0000h,spcr2            ; start ptr = 0, end ptr = 0
                move.w  #8101h,spcr1            ; enable QSPI

        p_on_1: btst.b  #7,spcr4                ; test if word has finished sending
                beq.b   p_on_1

                movem.l (sp)+,d0-d7/a0-a6       ; restore registers used

             rts

*-----------------------------------------------------------------------------+
*  Function:   PowerOff()                                                     |
*  Purpose :   Turn of flash programming voltage.                             |
*  Synopsis:   void  PowerOff( void );                                        |
*  Input   :   None.                                                          |
*  Output  :   None.                                                          |
*  Register:                                                                  |
*  Comments:                                                                  |
*-----------------------------------------------------------------------------+
        PowerOff:

                bclr.b  #6,porte                ; set WP#, unlock boot block of flash

                move.b  #00h,led                ; set Vpp enable bit in fpga
                move.b  #66h,encdr              ; write 99h to enable shift register in fpga
                move.w  #0a302h,spcr0           ; master, 8 bit xfer, 4.19MHz
                move.w  #0000h,spcr2            ; set end ptr = 0, start ptr = 0
                move.w  #0000h,spcr3            ; QSPI not finished, normal mode
                move.w  #0101h,spcr1            ; disable qspi
                move.b  #02h,qpar               ; set mosi as qspi pin

                move.b  #30h,cram0              ; initialize command ram  
                move.b  #99h,tr01               ; move opposite of enc(0-7)into tran_ram0
                move.w  #0000h,spcr3            ; qspi not finished, normal mode
                move.w  #0000h,spcr2            ; start ptr = 0, end ptr = 0
                move.w  #8101h,spcr1            ; enable QSPI

        p_off_1:btst.b  #7,spcr4                ; test if word has finished sending
                beq.b   p_off_1

                clr.l   d0                      ; flag OK

             rts


*-----------------------------------------------------------------------------+
*  Function:   Erase()                                                        |
*  Purpose :   Erase a block of flash.                                        |
*  Synopsis:   void  Erase( char* );                                          |
*  Input   :   a0 -> address within block to erase                            |
*  Output  :   None.                                                          |
*  Register:                                                                  |
*  Comments:                                                                  |
*-----------------------------------------------------------------------------+
        Erase:
                move.w  #0020h,(a0)             ; erase command
                move.w  #00d0h,(a0)             ; .

        Erase01:
                _watchdog                       ; wait till ready
                move.w  (a0),d0
                andi.w  #0080h,d0
                beq.s   Erase01

                move.w  #$00ff,(a0)             ; Return to read mode

                rts

*-----------------------------------------------------------------------------+
*  Function:   Write()                                                        |
*  Purpose :   Write a block of words into flash.                             |
*  Synopsis:   void  Write( dst, src, n );                                    |
*  Input   :   a0 -> source                                                   |
*              a1 -> destination                                              |
*              d1 -> holds count (number of words)                            |
*  Output  :   None.                                                          |
*  Register:                                                                  |
*  Comments:                                                                  |
*-----------------------------------------------------------------------------+
        Write:
                move.w  #0040h,(a1)             ; write command
                move.w  (a0)+,(a1)+             ; write data to flash

        Write01:
                _watchdog                       ; wait for status
                move.w  #0070h,(-2,a1)          ; read status command
                move.w  (-2,a1),d2
                andi.w  #0080h,d2
                beq.b   Write01

                subq.l  #1,d1                   ; update/check word counter
                bpl.b   Write

                move.w  #$00ff,(-2,a1)          ; Return to read mode

                rts

*-----------------------------------------------------------------------------+
*  Function:   Verify()                                                       |
*  Purpose :   Verify a block of long words in flash.                         |
*  Synopsis:   itn   Verify( dst, src, n );                                   |
*  Input   :   a0 -> source                                                   |
*              a1 -> destination                                              |
*              d1 -> holds count (number of long words)                       |
*  Output  :   None.                                                          |
*  Register:                                                                  |
*  Comments:                                                                  |
*-----------------------------------------------------------------------------+
        Verify:
                _watchdog
                cmp.l   (a0)+,(a1)+             ; compare word
                bne.b   VerifyErr
                subq.l  #1,d1                   ; update/check word counter
                bgt.b   Verify                  ; if greater than zero
                move.l  #000000000h,d0          ; flag OK
                bra.b   Verify_exit

        VerifyErr:
                move.l  #0ffffffffh,d0          ; flag error

        Verify_exit:
                rts


*-----------------------------------------------------------------------------+
*                       End Flash programming utilities                       |
*-----------------------------------------------------------------------------+

      _Boot_CodeEnd:   equ     *               ; end code

